import React, { useState } from 'react';
import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';
import Step4 from './Step4';
import Step5 from './Step5';
import Step6 from './Step6';
import Step7 from './Step7';
import Step8 from './Step8';
import Step9 from './Step9';
import Step10 from './Step10';
import Step11 from './Step11';
import Step12 from './Step12';
import Step13 from './Step13';
import { collection, addDoc } from "firebase/firestore";
import { db, storage } from '../firebase';
import { uploadBytesResumable, getDownloadURL, ref } from 'firebase/storage';

function Application() {
    const [currentStep, setCurrentStep] = useState(1);
    const [formData, setFormData] = useState({});
    const [formSubmitted, setFormSubmitted] = useState(false);

    const updateData = (step, data) => {
        const updatedFormData = { ...formData, ...data };
        console.log("Updated form data: ", updatedFormData);
        setFormData(updatedFormData);
        setCurrentStep(prevStep => prevStep + 1);

        if (step === 13) {
        submitForm(updatedFormData);
        setFormSubmitted(true);
        }
    };

    const goBack = () => {
        setCurrentStep(prevStep => prevStep - 1);
    };

    const submitForm = async (data) => {
        // Copy data to a new object because we shouldn't mutate props
        const newData = { ...data };

        try {
            // Assuming newData.photoID and newData.additionalID are the file objects

            // Upload photoID to Firebase Storage and get download URL
            let photoIDRef = ref(storage, 'photoID/' + newData.photoID.name);
            await uploadBytesResumable(photoIDRef, newData.photoID);
            newData.photoID = await getDownloadURL(photoIDRef);

            // Do the same for additionalID
            // let additionalIDRef = ref(storage, 'additionalID/' + newData.additionalID.name);
            // await uploadBytesResumable(additionalIDRef, newData.additionalID);
            // newData.additionalID = await getDownloadURL(additionalIDRef);

            const docRef = await addDoc(collection(db, "application"), newData);
            console.log("Document written with ID: ", docRef.id);
        } catch (e) {
            console.error("Error adding document: ", e);
        }
    };


    const renderStep = () => {
        if (formSubmitted) {
            return (
            <div className="flex flex-col items-center justify-center h-screen bg-green-50">
                <h2 className="mb-4 text-3xl font-semibold text-green-800">Application Submitted Successfully!</h2>
                <p className="max-w-md text-center text-lg text-green-700">
                    Your application has been received and is under review.
                    You will be notified via email when the review is complete.
                </p>
            </div>

            );
        }
        switch (currentStep) {
        case 1:
            return <Step1 updateData={updateData} formData={formData} />;
        case 2:
            return <Step2 updateData={updateData} goBack={goBack} formData={formData} />;
        case 3:
            return <Step3 updateData={updateData} goBack={goBack} formData={formData} />;
        case 4:
            return <Step4 updateData={updateData} goBack={goBack} formData={formData} />;
        case 5:
            return <Step5 updateData={updateData} goBack={goBack} formData={formData} />;
        case 6:
            return <Step6 updateData={updateData} goBack={goBack} formData={formData} />;
        case 7:
            return <Step7 updateData={updateData} goBack={goBack} formData={formData} />;
        case 8:
            return <Step8 updateData={updateData} goBack={goBack} formData={formData} />;
        case 9:
            return <Step9 updateData={updateData} goBack={goBack} formData={formData} />;
        case 10:
            return <Step10 updateData={updateData} goBack={goBack} formData={formData} />;
        case 11:
            return <Step11 updateData={updateData} goBack={goBack} formData={formData} />;
        case 12:
            return <Step12 updateData={updateData} goBack={goBack} formData={formData} />;
        case 13:
            return <Step13 updateData={updateData} goBack={goBack} formData={formData} />;
        default:
            return null;
        }
    };

    return (
        <div className="pt-10">
        {renderStep()}
        </div>
    );
}

export default Application;




// import React, { useState } from 'react';
// import Step1 from './Step1';
// import Step2 from './Step2';
// import Step3 from './Step3';
// import Step4 from './Step4';
// import Step5 from './Step5';
// import Step6 from './Step6';
// import Step7 from './Step7';
// import Step8 from './Step8';
// import Step9 from './Step9';
// import Step10 from './Step10';
// import Step11 from './Step11';
// import Step12 from './Step12';
// import { collection, addDoc } from "firebase/firestore";
// import { db, storage } from '../firebase';
// import { uploadBytesResumable, getDownloadURL } from 'firebase/storage';

// function Application() {
//     const [currentStep, setCurrentStep] = useState(1);
//     const [formData, setFormData] = useState({});

//     const updateData = (step, data) => {
//         const updatedFormData = { ...formData, ...data };
//         console.log("Updated form data: ", updatedFormData);
//         setFormData(updatedFormData);
//         setCurrentStep(prevStep => prevStep + 1);

//         if (step === 12) {
//         submitForm(updatedFormData);
//         }
//     };

//     const goBack = () => {
//         setCurrentStep(prevStep => prevStep - 1);
//     };

//     const submitForm = async (data) => {
//         // Submit the form here
//         try {
//         const docRef = await addDoc(collection(db, "application"), data);
//         console.log("Document written with ID: ", docRef.id);
//         } catch (e) {
//         console.error("Error adding document: ", e);
//         }
//     };

//     const renderStep = () => {
//         switch (currentStep) {
//         case 1:
//             return <Step1 updateData={updateData} formData={formData} />;
//         case 2:
//             return <Step2 updateData={updateData} goBack={goBack} formData={formData} />;
//         case 3:
//             return <Step3 updateData={updateData} goBack={goBack} formData={formData} />;
//         case 4:
//             return <Step4 updateData={updateData} goBack={goBack} formData={formData} />;
//         case 5:
//             return <Step5 updateData={updateData} goBack={goBack} formData={formData} />;
//         case 6:
//             return <Step6 updateData={updateData} goBack={goBack} formData={formData} />;
//         case 7:
//             return <Step7 updateData={updateData} goBack={goBack} formData={formData} />;
//         case 8:
//             return <Step8 updateData={updateData} goBack={goBack} formData={formData} />;
//         case 9:
//             return <Step9 updateData={updateData} goBack={goBack} formData={formData} />;
//         case 10:
//             return <Step10 updateData={updateData} goBack={goBack} formData={formData} />;
//         case 11:
//             return <Step11 updateData={updateData} goBack={goBack} formData={formData} />;
//         case 12:
//             return <Step12 updateData={updateData} goBack={goBack} formData={formData} />;
//         default:
//             return null;
//         }
//     };

//     return (
//         <div className="pt-10">
//         {renderStep()}
//         </div>
//     );
// }

// export default Application;



// import React, { useState } from 'react';
// import Step1 from './Step1';
// import Step2 from './Step2';
// import { collection, addDoc } from "firebase/firestore";
// import { db } from '../firebase';

// function Application() {
//     const [currentStep, setCurrentStep] = useState(1);
//     const [formData, setFormData] = useState({});

//     const updateData = (step, data) => {
//         const updatedFormData = { ...formData, ...data };
//         console.log("Updated form data: ", updatedFormData);
//         setFormData(updatedFormData);
//         setCurrentStep(prevStep => prevStep + 1);

//         if (step === 2) {
//             submitForm(updatedFormData);
//         }
//     };

//     const submitForm = async (data) => {
//         // Submit the form here
//         try {
//             const docRef = await addDoc(collection(db, "application"), data);
//             console.log("Document written with ID: ", docRef.id);
//         } catch (e) {
//             console.error("Error adding document: ", e);
//         }
//     };

//     return (
//         <div className='pt-10'>
//             {currentStep === 1 && <Step1 updateData={updateData} />}
//             {currentStep === 2 && <Step2 updateData={updateData} />}
//         </div>
//     );
// }

// export default Application;










// import React, { useState } from 'react';
// import Step1 from './Step1';
// import Step2 from './Step2';
// import { collection, addDoc } from "firebase/firestore";
// import { db } from '../firebase';

// function Application() {
//     const [currentStep, setCurrentStep] = useState(1);
//     const [formData, setFormData] = useState({});

//     const updateData = (step, data) => {
//         setFormData(prevData => {
//             const updatedData = { ...prevData, [step]: data };
//             console.log('Updated form data:', updatedData);
//             return updatedData;
//         });
//         setCurrentStep(prevStep => prevStep + 1);

//         if (step === 2) {
//             submitForm();
//         }
//     };


//     const submitForm = async () => {
//         // Flatten the form data
//         const flatData = { ...formData[1], ...formData[2] };

//         // Submit the form here
//         try {
//             const docRef = await addDoc(collection(db, "application"), flatData);
//             console.log("Document written with ID: ", docRef.id);
//         } catch (e) {
//             console.error("Error adding document: ", e);
//         }
//     };


//     return (
//         <div className='pt-10'>
//         {currentStep === 1 && <Step1 updateData={updateData} />}
//         {currentStep === 2 && <Step2 updateData={updateData} submitForm={submitForm} />}
//         </div>
//     );
// }

// export default Application;
