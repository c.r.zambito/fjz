import React, { useState } from "react";
import { addDoc, collection } from "firebase/firestore";
import { db } from "../firebase";

function Application() {
    const [firstName, setFirstName] = useState("");
    const [middleName, setMiddleName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [isFirstNameEmpty, setIsFirstNameEmpty] = useState(false);
    const [isLastNameEmpty, setIsLastNameEmpty] = useState(false);
    const [isEmailEmpty, setIsEmailEmpty] = useState(false);

    const submitForm = async (event) => {
        event.preventDefault();

        // Check if required fields are empty
        if (!firstName) {
        setIsFirstNameEmpty(true);
        }
        if (!lastName) {
        setIsLastNameEmpty(true);
        }
        if (!email) {
        setIsEmailEmpty(true);
        }

        if (firstName && lastName && email) {
        try {
            const docRef = await addDoc(collection(db, "application"), {
            "first name": firstName,
            "middle name": middleName,
            "last name": lastName,
            email: email,
            });

            console.log("Document written with ID: ", docRef.id);

            setFirstName("");
            setMiddleName("");
            setLastName("");
            setEmail("");
        } catch (error) {
            console.error("Error adding document: ", error);
        }
        }
    };

    return (
        <form onSubmit={submitForm} className="max-w-md mx-auto p-6 bg-white rounded shadow-md">
        <div className="mb-6">
            <label className="block mb-1 font-semibold text-gray-700">First Name</label>
            <input
            value={firstName}
            onChange={(e) => {
                setFirstName(e.target.value);
                setIsFirstNameEmpty(false);
            }}
            placeholder="First name"
            className={`w-full p-2 rounded ${isFirstNameEmpty ? "border-red-500" : "border-gray-300"}`}
            />
            {isFirstNameEmpty && (
            <div className="text-red-500 text-xs mt-1">Please enter your first name</div>
            )}
        </div>

        <div className="mb-6">
            <label className="block mb-1 font-semibold text-gray-700">Middle Name</label>
            <input
            value={middleName}
            onChange={(e) => setMiddleName(e.target.value)}
            placeholder="Middle name"
            className="w-full p-2 rounded border-gray-300"
            />
        </div>

        <div className="mb-6">
            <label className="block mb-1 font-semibold text-gray-700">Last Name</label>
            <input
            value={lastName}
            onChange={(e) => {
                setLastName(e.target.value);
                setIsLastNameEmpty(false);
            }}
            placeholder="Last name"
            className={`w-full p-2 rounded ${isLastNameEmpty ? "border-red-500" : "border-gray-300"}`}
            />
            {isLastNameEmpty && (
            <div className="text-red-500 text-xs mt-1">Please enter your last name</div>
            )}
        </div>

        <div className="mb-6">
            <label className="block mb-1 font-semibold text-gray-700">Email</label>
            <input
            value={email}
            onChange={(e) => {
                setEmail(e.target.value);
                setIsEmailEmpty(false);
            }}
            placeholder="Email"
            className={`w-full p-2 rounded ${isEmailEmpty ? "border-red-500" : "border-gray-300"}`}
            />
            {isEmailEmpty && (
            <div className="text-red-500 text-xs mt-1">Please enter your email</div>
            )}
        </div>

        <button
            type="submit"
            className="w-full py-2 px-4 mt-4 bg-blue-500 text-white rounded hover:bg-blue-600 transition-colors"
        >
            Submit
        </button>
        </form>
    );
}

export default Application;
