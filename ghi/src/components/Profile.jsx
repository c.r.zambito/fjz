import React from "react";
import construction from "../assets/underconstruction.png";

const Profile = () => {
    return (
        <div className="flex items-center justify-center h-screen bg-gray-100">
        <div className="p-8 bg-white rounded shadow-lg">
            <h1 className="text-3xl font-bold mb-4">Coming Soon!</h1>
            <p className="text-gray-600 mb-8">
            We're currently working on something awesome. Stay tuned!
            </p>
            <img
            src={construction}
            alt="Under Construction"
            className="w-64 h-auto mx-auto mb-8"
            />
            <div className="flex justify-center">
            <a
                href="https://fjzapartments.com/"
                className="bg-blue-500 hover:bg-blue-600 text-white font-semibold py-2 px-4 rounded"
            >
                Back to Home
            </a>
            </div>
        </div>
        </div>
    );
};

export default Profile;
