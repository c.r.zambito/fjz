import React, { useState } from 'react';

function Step1({ updateData, formData }) {
    const [firstName, setFirstName] = useState(formData.firstName || "");
    const [middleName, setMiddleName] = useState(formData.middleName || "");
    const [lastName, setLastName] = useState(formData.lastName || "");
    const [email, setEmail] = useState(formData.email || "");
    const [ssn, setSsn] = useState(formData.ssn || "");
    const [phone, setPhone] = useState(formData.phone || "");
    const [apt, setApt] = useState(formData.apt || "");
    const [dateTime, setDateTime] = useState(formData.dateTime || "");

    const [isFirstNameEmpty, setIsFirstNameEmpty] = useState(false);
    const [isLastNameEmpty, setIsLastNameEmpty] = useState(false);
    const [isEmailEmpty, setIsEmailEmpty] = useState(false);
    const [isSsnEmpty, setIsSsnEmpty] = useState(false);
    const [isPhoneEmpty, setIsPhoneEmpty] = useState(false);
    const [isAptEmpty, setIsAptEmpty] = useState(false);
    const [isDateTimeEmpty, setIsDateTimeEmpty] = useState(false)

    const handleClick = () => {
        const currentDateTime = new Date().toLocaleString('en-US', {
            timeZone: 'America/New_York',
        });
        setDateTime(currentDateTime);
        setIsDateTimeEmpty(false);
        };

    const nextStep = (event) => {
        event.preventDefault();

        // Check if required fields are empty
        if (!firstName) {
            setIsFirstNameEmpty(true);
        }
        if (!lastName) {
            setIsLastNameEmpty(true);
        }
        if (!email) {
            setIsEmailEmpty(true);
        }
        if (!ssn) {
            setIsSsnEmpty(true);
        }
        if (!phone) {
            setIsPhoneEmpty(true);
        }
        if (!apt) {
            setIsAptEmpty(true);
        }
        if (!dateTime) {
            setIsDateTimeEmpty(true);
        }

        if (firstName && lastName && email && ssn && phone && apt, dateTime) {
            // Then pass it back to the parent component:
            updateData(1, { firstName, middleName, lastName, email, ssn, phone, apt, dateTime });
            // submitForm(); // if this is the last step, you can call submitForm here
        }
    };

    return (
        <div className="container mx-auto max-w-md p-6 bg-white rounded shadow-md">
            <h2 className="mb-6 text-2xl font-semibold text-gray-700">Personal Information</h2>
            <div className="flex items-center">
                <div className="w-1/13 h-1 bg-blue-500"></div>
                <div className="w-12/13 h-1 bg-gray-300"></div>
            </div>
            <span className="text-gray-500">Step 1 of 13</span>
            <h3 className="mb-6 text-1xl font-semibold text-red-500">Do Not Refresh - Form Will Reset</h3>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">First Name*</label>
                <input
                    value={firstName}
                    onChange={(e) => {
                        setFirstName(e.target.value);
                        setIsFirstNameEmpty(false);
                    }}
                    placeholder="First name"
                    className={`w-full p-2 rounded ${isFirstNameEmpty ? "border-red-500" : "border-gray-300"}`}
                />
                {isFirstNameEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please enter your first name</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Middle Name</label>
                <input
                    value={middleName}
                    onChange={(e) => setMiddleName(e.target.value)}
                    placeholder="Middle name"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Last Name*</label>
                <input
                    value={lastName}
                    onChange={(e) => {
                        setLastName(e.target.value);
                        setIsLastNameEmpty(false);
                    }}
                    placeholder="Last name"
                    className={`w-full p-2 rounded ${isLastNameEmpty ? "border-red-500" : "border-gray-300"}`}
                />
                {isLastNameEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please enter your last name</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Email*</label>
                <input
                    value={email}
                    onChange={(e) => {
                        setEmail(e.target.value);
                        setIsEmailEmpty(false);
                    }}
                    placeholder="Email"
                    className={`w-full p-2 rounded ${isEmailEmpty ? "border-red-500" : "border-gray-300"}`}
                />
                {isEmailEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please enter your email</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Social Security Number*</label>
                <input
                    value={ssn}
                    onChange={(e) => {
                        setSsn(e.target.value);
                        setIsSsnEmpty(false);
                    }}
                    placeholder="Social Security Number"
                    type="password"
                    className={`w-full p-2 rounded ${isSsnEmpty ? "border-red-500" : "border-gray-300"}`}
                />
                {isSsnEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please enter your social security number</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Phone Number*</label>
                <input
                    value={phone}
                    onChange={(e) => {
                        setPhone(e.target.value);
                        setIsPhoneEmpty(false);
                    }}
                    placeholder="Phone Number"
                    className={`w-full p-2 rounded ${isPhoneEmpty ? "border-red-500" : "border-gray-300"}`}
                />
                {isPhoneEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please enter your phone number</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Apartment Location and Apartment Number*</label>
                <input
                    value={apt}
                    onChange={(e) => {
                        setApt(e.target.value);
                        setIsAptEmpty(false);
                    }}
                    placeholder="Apartment Location and Apartment Number"
                    className={`w-full p-2 rounded ${isAptEmpty ? "border-red-500" : "border-gray-300"}`}
                />
                {isPhoneEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please enter apartment location and apartment number</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Date and Time*</label>
                <div className="flex">
                    <input
                    value={dateTime}
                    onChange={(e) => {
                        setDateTime(e.target.value);
                        setIsDateTimeEmpty(false);
                    }}
                    placeholder="Date and Time"
                    className={`w-full p-2 rounded ${isDateTimeEmpty ? 'border-red-500' : 'border-gray-300'}`}
                    />
                    <button
                    type="button"
                    className="ml-2 px-2 py-1 bg-gray-500 text-white rounded hover:bg-blue-600"
                    onClick={handleClick}
                    >
                    Fill with Current Date & Time
                    </button>
                </div>
                {isDateTimeEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please enter the date and time</div>
                )}
            </div>

            <button onClick={nextStep} className="w-full py-2 px-4 mt-4 bg-blue-500 text-white rounded hover:bg-blue-600 transition-colors">Next</button>
        </div>
    );
}

export default Step1;
