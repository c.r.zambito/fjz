import React, { useState } from 'react'

function Step10({ updateData, goBack, formData }) {
    const [referenceContact1, setReferenceContact1] = useState(formData.referenceContact1 || "");
    const [referenceContactPhone1, setReferenceContactPhone1] = useState(formData.referenceContactPhone1 || "");
    const [referenceContactRelationship1, setReferenceContactRelationship1] = useState(formData.referenceContactRelationship1 || "");
    const [referenceContactAddress1, setReferenceContactAddress1] = useState(formData.referenceContactAddress1 || "");

    const [referenceContact2, setReferenceContact2] = useState(formData.referenceContact2 || "");
    const [referenceContactPhone2, setReferenceContactPhone2] = useState(formData.referenceContactPhone2 || "");
    const [referenceContactRelationship2, setReferenceContactRelationship2] = useState(formData.referenceContactRelationship2 || "");
    const [referenceContactAddress2, setReferenceContactAddress2] = useState(formData.referenceContactAddress2 || "");


    const [isReferenceContactEmpty1, setIsReferenceContactEmpty1] = useState(false);




    const nextStep = (event) => {
        event.preventDefault();

        // Check if required fields are empty
        if (referenceContact1.trim() === "") {
            setIsReferenceContactEmpty1(true);
        }



        if ( referenceContact1 ) {
            // Log the data to the console to verify it's being updated
            console.log(referenceContact1.trim() !== "");

            // Then pass it back to the parent component:
            updateData(10, { referenceContact1, referenceContactPhone1: referenceContactPhone1, referenceContactRelationship1: referenceContactRelationship1, referenceContactAddress1: referenceContactAddress1, referenceContact2: referenceContact2, referenceContactPhone2: referenceContactPhone2, referenceContactRelationship2: referenceContactRelationship2, referenceContactAddress2: referenceContactAddress2 });
        }
    };


    return (
        <div className="container mx-auto max-w-md p-6 bg-white rounded shadow-md">
            <h2 className="mb-6 text-2xl font-semibold text-gray-700">Personal Reference</h2>


            <div className="flex items-center">
                <div className="w-10/13 h-1 bg-blue-500"></div>
                <div className="w-3/13 h-1 bg-gray-300"></div>
            </div>
            <span className="text-gray-500">Step 10 of 13</span>


            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700 pt-4">Personal Reference (1)*</label>
                <input
                    value={referenceContact1}
                    onChange={(e) => {
                        setReferenceContact1(e.target.value);
                        setIsReferenceContactEmpty1(false);
                    }}
                    placeholder="Personal Reference (1)"
                    className={`w-full p-2 rounded ${isReferenceContactEmpty1 ? "border-red-500" : "border-gray-300"}`}
                />
                {isReferenceContactEmpty1 && (
                    <div className="text-red-500 text-xs mt-1">Please enter your personal reference contact</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Personal Reference Contact Phone (1)</label>
                <input
                    type="text"
                    value={referenceContactPhone1}
                    onChange={(e) => {
                        setReferenceContactPhone1(e.target.value);
                    }}
                    placeholder="Personal Reference Contact Phone (1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Personal Reference Contact Relationship (1)</label>
                <input
                    type="text"
                    value={referenceContactRelationship1}
                    onChange={(e) => {
                        setReferenceContactRelationship1(e.target.value);
                    }}
                    placeholder="Personal Reference Contact Relationship (1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Personal Reference Contact Address(1)</label>
                <input
                    type="text"
                    value={referenceContactAddress1}
                    onChange={(e) => {
                        setReferenceContactAddress1(e.target.value);
                    }}
                    placeholder="Personal Reference Contact Address(1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>


            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Personal Reference Contact (2)</label>
                <input
                    type="text"
                    value={referenceContact2}
                    onChange={(e) => {
                        setReferenceContact2(e.target.value);
                    }}
                    placeholder="Personal Reference Contact (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Personal Reference Contact Phone (2)</label>
                <input
                    type="text"
                    value={referenceContactPhone2}
                    onChange={(e) => {
                        setReferenceContactPhone2(e.target.value);
                    }}
                    placeholder="Personal Reference Contact Phone (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Personal Reference Contact Relationship (2)</label>
                <input
                    type="text"
                    value={referenceContactRelationship2}
                    onChange={(e) => {
                        setReferenceContactRelationship2(e.target.value);
                    }}
                    placeholder="Personal Reference Contact Relationship (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Personal Reference Contact Address (2)</label>
                <input
                    type="text"
                    value={referenceContactAddress2}
                    onChange={(e) => {
                        setReferenceContactAddress2(e.target.value);
                    }}
                    placeholder="Personal Reference Contact Address (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>






            <button onClick={goBack} className="w-full py-2 px-4 mt-2 bg-gray-500 text-white rounded hover:bg-gray-600 transition-colors">Back</button>
            <button onClick={nextStep} className="w-full py-2 px-4 mt-4 bg-blue-500 text-white rounded hover:bg-blue-600 transition-colors">Next</button>
        </div>
    );
}

export default Step10
