import React, { useState } from 'react'

function Step11({ updateData, goBack, formData }) {
    const [applicantSued, setApplicantSued] = useState(formData.applicantSued || "");
    const [applicantBankrupt, setApplicantBankrupt] = useState(formData.applicantBankrupt || "");
    const [applicantFelony, setApplicantFelony] = useState(formData.applicantFelony || "");
    const [applicantBroken, setApplicantBroken] = useState(formData.applicantBroken || "");
    const [applicantSheriff, setApplicantSheriff] = useState(formData.applicantSheriff || "");
    const [applicantCourt, setApplicantCourt] = useState(formData.applicantCourt || "");
    const [applicantMoved, setApplicantMoved] = useState(formData.applicantMoved || "");
    const [applicantRent, setApplicantRent] = useState(formData.applicantRent || "");

    const [isApplicantSuedEmpty, setIsApplicantSuedEmpty] = useState(false);
    const [isApplicantBankruptEmpty, setIsApplicantBankruptEmpty] = useState(false);
    const [isApplicantFelonyEmpty, setIsApplicantFelonyEmpty] = useState(false);
    const [isApplicantBrokenEmpty, setIsApplicantBrokenEmpty] = useState(false);
    const [isApplicantSheriffEmpty, setIsApplicantSheriffEmpty] = useState(false);
    const [isApplicantCourtEmpty, setIsApplicantCourtEmpty] = useState(false);
    const [isApplicantMovedEmpty, setIsApplicantMovedEmpty] = useState(false);
    const [isApplicantRentEmpty, setIsApplicantRentEmpty] = useState(false);




    const nextStep = (event) => {
        event.preventDefault();

        // Check if required fields are empty
        if (applicantSued.trim() === "") {
            setIsApplicantSuedEmpty(true);
        }
        if (applicantBankrupt.trim() === "") {
            setIsApplicantBankruptEmpty(true);
        }
        if (applicantFelony.trim() === "") {
            setIsApplicantFelonyEmpty(true);
        }
        if (applicantBroken.trim() === "") {
            setIsApplicantBrokenEmpty(true);
        }
        if (applicantSheriff.trim() === "") {
            setIsApplicantSheriffEmpty(true);
        }
        if (applicantCourt.trim() === "") {
            setIsApplicantCourtEmpty(true);
        }
        if (applicantMoved.trim() === "") {
            setIsApplicantMovedEmpty(true);
        }
        if (applicantRent.trim() === "") {
            setIsApplicantRentEmpty(true);
        }



        if ( applicantSued && applicantBankrupt && applicantFelony && applicantBroken && applicantSheriff && applicantCourt && applicantMoved && applicantRent) {
            // Log the data to the console to verify it's being updated
            console.log(applicantSued.trim() !== "");

            // Then pass it back to the parent component:
            updateData(11, { applicantSued, applicantBankrupt, applicantFelony, applicantBroken, applicantSheriff, applicantCourt, applicantMoved, applicantRent });
        }
    };


    return (
        <div className="container mx-auto max-w-md p-6 bg-white rounded shadow-md">
            <h2 className="mb-6 text-2xl font-semibold text-gray-700">Applicant Questionnaire</h2>


            <div className="flex items-center">
                <div className="w-11/13 h-1 bg-blue-500"></div>
                <div className="w-2/13 h-1 bg-gray-300"></div>
            </div>
            <span className="text-gray-500">Step 11 of 13</span>


            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700 pt-4">Has Applicant Ever Been Sued For Bills?*</label>
                <select
                    value={applicantSued}
                    onChange={(e) => {
                        setApplicantSued(e.target.value);
                        setIsApplicantSuedEmpty(false);
                    }}
                    className={`w-full p-2 rounded ${isApplicantSuedEmpty ? "border-red-500" : "border-gray-300"}`}
                >
                    <option value="">Select an option</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
                {isApplicantSuedEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please select an option</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Has Applicant Ever Been Bankrupt*</label>
                <select
                    value={applicantBankrupt}
                    onChange={(e) => {
                        setApplicantBankrupt(e.target.value);
                        setIsApplicantBankruptEmpty(false);
                    }}
                    className={`w-full p-2 rounded ${isApplicantBankruptEmpty ? "border-red-500" : "border-gray-300"}`}
                >
                    <option value="">Select an option</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
                {isApplicantBankruptEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please select an option</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Has Applicant Ever Been Guilty Of A Felony*</label>
                <select
                    value={applicantFelony}
                    onChange={(e) => {
                        setApplicantFelony(e.target.value);
                        setIsApplicantFelonyEmpty(false);
                    }}
                    className={`w-full p-2 rounded ${isApplicantFelonyEmpty ? "border-red-500" : "border-gray-300"}`}
                >
                    <option value="">Select an option</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
                {isApplicantFelonyEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please select an option</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Has Applicant Ever Broken A Lease*</label>
                <select
                    value={applicantBroken}
                    onChange={(e) => {
                        setApplicantBroken(e.target.value);
                        setIsApplicantBrokenEmpty(false);
                    }}
                    className={`w-full p-2 rounded ${isApplicantBrokenEmpty ? "border-red-500" : "border-gray-300"}`}
                >
                    <option value="">Select an option</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
                {isApplicantBrokenEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please select an option</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Has Applicant Ever Been Locked Out Of Their Apartment By The Sheriff*</label>
                <select
                    value={applicantSheriff}
                    onChange={(e) => {
                        setApplicantSheriff(e.target.value);
                        setIsApplicantSheriffEmpty(false);
                    }}
                    className={`w-full p-2 rounded ${isApplicantSheriffEmpty ? "border-red-500" : "border-gray-300"}`}
                >
                    <option value="">Select an option</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
                {isApplicantSheriffEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please select an option</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Has Applicant Ever Been Brought To Court By Another Landlord?*</label>
                <select
                    value={applicantCourt}
                    onChange={(e) => {
                        setApplicantCourt(e.target.value);
                        setIsApplicantCourtEmpty(false);
                    }}
                    className={`w-full p-2 rounded ${isApplicantCourtEmpty ? "border-red-500" : "border-gray-300"}`}
                >
                    <option value="">Select an option</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
                {isApplicantCourtEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please select an option</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Has Applicant Ever Moved Owing Rent Or Damaged An Apartment?*</label>
                <select
                    value={applicantMoved}
                    onChange={(e) => {
                        setApplicantMoved(e.target.value);
                        setIsApplicantMovedEmpty(false);
                    }}
                    className={`w-full p-2 rounded ${isApplicantMovedEmpty ? "border-red-500" : "border-gray-300"}`}
                >
                    <option value="">Select an option</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
                {isApplicantMovedEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please select an option</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Is The Total Move-In Amount Available Now (Rent And Deposit)?*</label>
                <select
                    value={applicantRent}
                    onChange={(e) => {
                        setApplicantRent(e.target.value);
                        setIsApplicantRentEmpty(false);
                    }}
                    className={`w-full p-2 rounded ${isApplicantRentEmpty ? "border-red-500" : "border-gray-300"}`}
                >
                    <option value="">Select an option</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
                {isApplicantRentEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please select an option</div>
                )}
            </div>



            <button onClick={goBack} className="w-full py-2 px-4 mt-2 bg-gray-500 text-white rounded hover:bg-gray-600 transition-colors">Back</button>
            <button onClick={nextStep} className="w-full py-2 px-4 mt-4 bg-blue-500 text-white rounded hover:bg-blue-600 transition-colors">Next</button>
        </div>
    );
}

export default Step11
