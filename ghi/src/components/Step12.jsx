import React, { useState } from 'react';

function Step12({ updateData, goBack, formData }) {
    const [photoID, setPhotoID] = useState(formData.photoID || null);
    const [isPhotoIDEmpty, setIsPhotoIDEmpty] = useState(false);

    const handlePhotoID = (e) => {
        setPhotoID(e.target.files[0]);
        setIsPhotoIDEmpty(false);
    };

    const nextStep = (event) => {
        event.preventDefault();

        if (!photoID) {
            setIsPhotoIDEmpty(true);
        } else {
            console.log("File name:", photoID.name);

            // Update parent component state:
            updateData(12, { photoID });
        }
    };

    return (
        <div className="container mx-auto max-w-md p-6 bg-white rounded shadow-md">
            <h2 className="mb-6 text-2xl font-semibold text-gray-700">Upload ID</h2>


            <div className="flex items-center">
                <div className="w-12/13 h-1 bg-blue-500"></div>
                <div className="w-1/13 h-1 bg-gray-300"></div>
            </div>
            <span className="text-gray-500">Step 12 of 13</span>


            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Photo ID (Driver's License, Passport)*</label>
                <input
                    type="file"
                    onChange={handlePhotoID}
                    className={`w-full p-2 rounded ${isPhotoIDEmpty ? "border-red-500" : "border-gray-300"}`}
                />
                {isPhotoIDEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please upload your photo ID</div>
                )}
            </div>

            <button onClick={goBack} className="w-full py-2 px-4 mt-2 bg-gray-500 text-white rounded hover:bg-gray-600 transition-colors">Back</button>
            <button onClick={nextStep} className="w-full py-2 px-4 mt-4 bg-blue-500 text-white rounded hover:bg-blue-600 transition-colors">Next</button>
        </div>
    );
}

export default Step12;






// import React, { useState } from 'react';

// function Step12({ updateData, goBack, formData }) {
//     const [photoID, setPhotoID] = useState(formData.photoID || null);
//     const [additionalID, setAdditionalID] = useState(formData.additionalID || null);
//     const [isPhotoIDEmpty, setIsPhotoIDEmpty] = useState(false);

//     const handlePhotoID = (e) => {
//         setPhotoID(e.target.files[0]);
//         setIsPhotoIDEmpty(false);
//     };

//     const handleAdditionalID = (e) => {
//         setAdditionalID(e.target.files[0]);
//     };

//     const nextStep = (event) => {
//         event.preventDefault();

//         if (!photoID) {
//             setIsPhotoIDEmpty(true);
//         }

//         if (photoID) {
//             console.log("File name:", photoID.name, additionalID ? additionalID.name : "No additional ID provided");

//             // Update parent component state:
//             updateData(12, { photoID, additionalID });
//         }
//     };

//     return (
//         <div className="container mx-auto max-w-md p-6 bg-white rounded shadow-md">
//             <h2 className="mb-6 text-2xl font-semibold text-gray-700">Upload IDs</h2>

//             <div className="mb-6">
//                 <label className="block mb-1 font-semibold text-gray-700">Photo ID (Driver's License, Passport)*</label>
//                 <input
//                     type="file"
//                     onChange={handlePhotoID}
//                     className={`w-full p-2 rounded ${isPhotoIDEmpty ? "border-red-500" : "border-gray-300"}`}
//                 />
//                 {isPhotoIDEmpty && (
//                     <div className="text-red-500 text-xs mt-1">Please upload your photo ID</div>
//                 )}
//             </div>

//             <div className="mb-6">
//                 <label className="block mb-1 font-semibold text-gray-700">Additional Identification</label>
//                 <input
//                     type="file"
//                     onChange={handleAdditionalID}
//                     className="w-full p-2 rounded border-gray-300"
//                 />
//             </div>

//             <button onClick={goBack} className="w-full py-2 px-4 mt-2 bg-gray-500 text-white rounded hover:bg-gray-600 transition-colors">Back</button>
//             <button onClick={nextStep} className="w-full py-2 px-4 mt-4 bg-blue-500 text-white rounded hover:bg-blue-600 transition-colors">Next</button>
//         </div>
//     );
// }

// export default Step12;





// import React, { useState } from 'react';

// function Step12({ updateData, goBack, formData }) {
//     const [photoID, setPhotoID] = useState(formData.photoID || null);
//     const [additionalID, setAdditionalID] = useState(formData.additionalID || null);

//     const handlePhotoID = (e) => {
//         setPhotoID(e.target.files[0]);
//     };

//     const handleAdditionalID = (e) => {
//         setAdditionalID(e.target.files[0]);
//     };

//     const nextStep = (event) => {
//         event.preventDefault();

//         if (photoID && additionalID) {
//             console.log("File names:", photoID.name, additionalID.name);

//             // Update parent component state:
//             updateData(12, { photoID, additionalID });
//         }
//     };

//     return (
//         <div className="container mx-auto max-w-md p-6 bg-white rounded shadow-md">
//             <h2 className="mb-6 text-2xl font-semibold text-gray-700">Upload IDs</h2>

//             <div className="mb-6">
//                 <label className="block mb-1 font-semibold text-gray-700">Photo ID (Driver's License, Passport)*</label>
//                 <input
//                     type="file"
//                     onChange={handlePhotoID}
//                     className="w-full p-2 rounded border-gray-300"
//                 />
//             </div>

//             <div className="mb-6">
//                 <label className="block mb-1 font-semibold text-gray-700">Additional Identification</label>
//                 <input
//                     type="file"
//                     onChange={handleAdditionalID}
//                     className="w-full p-2 rounded border-gray-300"
//                 />
//             </div>

//             <button onClick={goBack} className="w-full py-2 px-4 mt-2 bg-gray-500 text-white rounded hover:bg-gray-600 transition-colors">Back</button>
//             <button onClick={nextStep} className="w-full py-2 px-4 mt-4 bg-blue-500 text-white rounded hover:bg-blue-600 transition-colors">Next</button>
//         </div>
//     );
// }

// export default Step12;
