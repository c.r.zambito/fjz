import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

function Step13({ updateData, goBack, formData }) {
    const [signature, setSignature] = useState(formData.signature || "");
    const [date, setDate] = useState(formData.date || "");
    const [isFormSubmitted, setIsFormSubmitted] = useState(false);
    const [isSignatureEmpty, setIsSignatureEmpty] = useState(false);

    const navigate = useNavigate();

    const handleDate = () => {
        const currentDate = new Date();
        const dateString = `${currentDate.getFullYear()}-${currentDate.getMonth() + 1}-${currentDate.getDate()}`;
        setDate(dateString);
    };

    const nextStep = (event) => {
        event.preventDefault();

        // Check if required fields are empty
        if (!signature) {
            setIsSignatureEmpty(true);
        }

        if (signature && date) {
            // Log the data to the console to verify it's being updated
            console.log(signature, date);

            // Then pass it back to the parent component:
            updateData(13, { signature, date });

            // Change form submitted state to true
            setIsFormSubmitted(true);
        }
    };

    if (isFormSubmitted) {
        return (
            <div className="container mx-auto max-w-md p-6 bg-white rounded shadow-md">
                <h2 className="text-2xl font-bold mb-4 text-center">Form Submitted Successfully!</h2>
                <p className="mb-6 text-center">Your rental application has been received and is under review.</p>
                <button
                    className="w-full py-2 px-4 bg-blue-500 text-white rounded hover:bg-blue-600 transition-colors"
                    onClick={() => navigate('/')}
                >
                    Continue
                </button>
            </div>
        );
    }

    return (
        <div className="container mx-auto max-w-md p-6 bg-white rounded shadow-md">
            <h2 className="mb-6 text-2xl font-semibold text-gray-700">Confirmation</h2>


            <div className="flex items-center">
                <div className="w-full h-1 bg-blue-500"></div>
                </div>
            <span className="text-gray-500">Step 13 of 13</span>


            <p className='pt-4'>
                Applicant authorizes the landlord to contact past and present landlords, employers,
                creditors, credit bureaus, neighbors and any other sources deemed necessary to investigate applicant.
                All information is true, accurate and complete to the best of applicant's knowledge.
                Landlord reserves the right to disqualify tenant if information is not as represented.
                ANY PERSON OR FIRM IS AUTHORIZED TO RELEASE INFORMATION ABOUT THE UNDERSIGNED UPON PRESENTATION OF THIS FORM OR A PHOTOCOPY OF THIS FORM AT ANY TIME.
            </p>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700 pt-4">Signature*</label>
                <input
                    value={signature}
                    onChange={(e) => {
                        setSignature(e.target.value);
                        setIsSignatureEmpty(false);
                    }}
                    placeholder="Type your full name"
                    className={`w-full p-2 rounded ${isSignatureEmpty ? "border-red-500" : "border-gray-300"}`}
                />
                {isSignatureEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please enter your signature</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Date*</label>
                <input
                    value={date}
                    readOnly
                    className={`w-full p-2 rounded "border-gray-300"`}
                />
                <button onClick={handleDate} className="w-full py-2 px-4 mt-2 bg-blue-500 text-white rounded hover:bg-blue-600 transition-colors">Fill date</button>
            </div>

            <button onClick={goBack} className="w-full py-2 px-4 mt-2 bg-gray-500 text-white rounded hover:bg-gray-600 transition-colors">Back</button>
            <button onClick={nextStep} className="w-full py-2 px-4 mt-4 bg-blue-500 text-white rounded hover:bg-blue-600 transition-colors">Submit</button>
        </div>
    );
}

export default Step13;




// import React, { useState } from 'react';

// function Step13({ updateData, goBack, formData }) {
//     const [signature, setSignature] = useState(formData.signature || "");
//     const [date, setDate] = useState(formData.date || "");

//     const [isSignatureEmpty, setIsSignatureEmpty] = useState(false);

//     const handleDate = () => {
//         const currentDate = new Date();
//         const dateString = `${currentDate.getFullYear()}-${currentDate.getMonth() + 1}-${currentDate.getDate()}`;
//         setDate(dateString);
//     };

//     const nextStep = (event) => {
//         event.preventDefault();

//         // Check if required fields are empty
//         if (!signature) {
//             setIsSignatureEmpty(true);
//         }

//         if (signature && date) {
//             // Log the data to the console to verify it's being updated
//             console.log(signature, date);

//             // Then pass it back to the parent component:
//             updateData(13, { signature, date });
//         }
//     };

//     return (
//         <div className="container mx-auto max-w-md p-6 bg-white rounded shadow-md">
//             <h2 className="mb-6 text-2xl font-semibold text-gray-700">Confirmation</h2>

//             <p>
//                 Applicant authorizes the landlord to contact past and present landlords, employers,
//                 creditors, credit bureaus, neighbors and any other sources deemed necessary to investigate applicant.
//                 All information is true, accurate and complete to the best of applicant's knowledge.
//                 Landlord reserves the right to disqualify tenant if information is not as represented.
//                 ANY PERSON OR FIRM IS AUTHORIZED TO RELEASE INFORMATION ABOUT THE UNDERSIGNED UPON PRESENTATION OF THIS FORM OR A PHOTOCOPY OF THIS FORM AT ANY TIME.
//             </p>

//             <div className="mb-6">
//                 <label className="block mb-1 font-semibold text-gray-700">Signature*</label>
//                 <input
//                     value={signature}
//                     onChange={(e) => {
//                         setSignature(e.target.value);
//                         setIsSignatureEmpty(false);
//                     }}
//                     placeholder="Type your full name"
//                     className={`w-full p-2 rounded ${isSignatureEmpty ? "border-red-500" : "border-gray-300"}`}
//                 />
//                 {isSignatureEmpty && (
//                     <div className="text-red-500 text-xs mt-1">Please enter your signature</div>
//                 )}
//             </div>

//             <div className="mb-6">
//                 <label className="block mb-1 font-semibold text-gray-700">Date*</label>
//                 <input
//                     value={date}
//                     readOnly
//                     className={`w-full p-2 rounded "border-gray-300"`}
//                 />
//                 <button onClick={handleDate} className="w-full py-2 px-4 mt-2 bg-blue-500 text-white rounded hover:bg-blue-600 transition-colors">Fill date</button>
//             </div>

//             <button onClick={goBack} className="w-full py-2 px-4 mt-2 bg-gray-500 text-white rounded hover:bg-gray-600 transition-colors">Back</button>
//             <button onClick={nextStep} className="w-full py-2 px-4 mt-4 bg-blue-500 text-white rounded hover:bg-blue-600 transition-colors">Submit</button>
//         </div>
//     );
// }

// export default Step13;
