import React, { useState } from 'react';

function Step2({ updateData, goBack, formData }) {
    const [dateOfBirth, setDateOfBirth] = useState(formData.dateOfBirth || "");
    const [maritalStatus, setMaritalStatus] = useState(formData.maritalStatus || "");
    const [driverLicenseNumber, setDriverLicenseNumber] = useState(formData.driverLicenseNumber || "");
    const [presentHomeAddress, setPresentHomeAddress] = useState(formData.presentHomeAddress || "");
    const [lengthOfTime, setLengthOfTime] = useState(formData.lengthOfTime || "");
    const [landlordPhone, setLandlordPhone] = useState(formData.landlordPhone || "");
    const [reasonForLeaving, setReasonForLeaving] = useState(formData.reasonForLeaving || "");
    const [rent, setRent] = useState(formData.rent || "");
    const [isRentUpToDate, setIsRentUpToDate] = useState(formData.isRentUpToDate || "");

    const [isDateOfBirthEmpty, setIsDateOfBirthEmpty] = useState(false);
    const [isMaritalStatusEmpty, setIsMaritalStatusEmpty] = useState(false);
    const [isDriverLicenseNumberEmpty, setIsDriverLicenseNumberEmpty] = useState(false);
    const [isPresentHomeAddressEmpty, setIsPresentHomeAddressEmpty] = useState(false);
    const [isLengthOfTimeEmpty, setIsLengthOfTimeEmpty] = useState(false);
    const [isRentEmpty, setIsRentEmpty] = useState(false);
    const [isRentUpToDateEmpty, setIsRentUpToDateEmpty] = useState(false);


    const nextStep = (event) => {
        event.preventDefault();

        // Check if required fields are empty
        if (!dateOfBirth) {
            setIsDateOfBirthEmpty(true);
        }
        if (!maritalStatus) {
            setIsMaritalStatusEmpty(true);
        }
        if (!driverLicenseNumber) {
            setIsDriverLicenseNumberEmpty(true);
        }
        if (!presentHomeAddress) {
            setIsPresentHomeAddressEmpty(true);
        }
        if (!lengthOfTime) {
            setIsLengthOfTimeEmpty(true);
        }
        if (!rent) {
            setIsRentEmpty(true);
        }
        if (!isRentUpToDate) {
            setIsRentUpToDateEmpty(true);
        }

        if (dateOfBirth && maritalStatus && driverLicenseNumber && presentHomeAddress && lengthOfTime && landlordPhone && reasonForLeaving && rent && isRentUpToDate) {
            // Log the data to the console to verify it's being updated
            console.log(dateOfBirth, maritalStatus, driverLicenseNumber, presentHomeAddress, lengthOfTime, landlordPhone, reasonForLeaving, rent, isRentUpToDate);

            // Then pass it back to the parent component:
            updateData(2, { dateOfBirth, maritalStatus, driverLicenseNumber, presentHomeAddress, lengthOfTime, landlordPhone, reasonForLeaving, rent, isRentUpToDate });
        }
    };


    return (
        <div className="container mx-auto max-w-md p-6 bg-white rounded shadow-md">
            <h2 className="mb-6 text-2xl font-semibold text-gray-700">Personal Information (continued)</h2>

            <div className="flex items-center">
                <div className="w-2/13 h-1 bg-blue-500"></div>
                <div className="w-11/13 h-1 bg-gray-300"></div>
            </div>
            <span className="text-gray-500">Step 2 of 13</span>


            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700 pt-4">Date of Birth*</label>
                <input
                    value={dateOfBirth}
                    onChange={(e) => {
                        setDateOfBirth(e.target.value);
                        setIsDateOfBirthEmpty(false);
                    }}
                    placeholder="YYYY-MM-DD"
                    type="date"
                    className={`w-full p-2 rounded ${isDateOfBirthEmpty ? "border-red-500" : "border-gray-300"}`}
                />
                {isDateOfBirthEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please enter your date of birth</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Marital Status</label>
                <select
                    value={maritalStatus}
                    onChange={(e) => {
                        setMaritalStatus(e.target.value);
                        setIsMaritalStatusEmpty(false);
                    }}
                    className={`w-full p-2 rounded ${isMaritalStatusEmpty ? "border-red-500" : "border-gray-300"}`}
                >
                    <option value="">--Please choose an option--</option>
                    <option value="single">Single</option>
                    <option value="married">Married</option>
                    <option value="divorced">Divorced</option>
                    <option value="widowed">Widowed</option>
                </select>
                {isMaritalStatusEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please select your marital status</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Driver License Number</label>
                <input
                    value={driverLicenseNumber}
                    onChange={(e) => {
                        setDriverLicenseNumber(e.target.value);
                        setIsDriverLicenseNumberEmpty(false);
                    }}
                    placeholder="Driver License Number"
                    className={`w-full p-2 rounded ${isDriverLicenseNumberEmpty ? "border-red-500" : "border-gray-300"}`}
                />
                {isDriverLicenseNumberEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please enter your driver license number</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Present Home Address*</label>
                <input
                    value={presentHomeAddress}
                    onChange={(e) => {
                        setPresentHomeAddress(e.target.value);
                        setIsPresentHomeAddressEmpty(false);
                    }}
                    placeholder="Address, CITY/STATE/ZIP"
                    className={`w-full p-2 rounded ${isPresentHomeAddressEmpty? "border-red-500" : "border-gray-300"}`}
                />
                {isPresentHomeAddressEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please enter your current home address</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Length of time at address*</label>
                <input
                    value={lengthOfTime}
                    onChange={(e) => {
                        setLengthOfTime(e.target.value);
                        setIsLengthOfTimeEmpty(false);
                    }}
                    placeholder="Length of time at address"
                    className={`w-full p-2 rounded ${isLengthOfTimeEmpty? "border-red-500" : "border-gray-300"}`}
                />
                {isPresentHomeAddressEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please enter length of time at address</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Landlord Phone*</label>
                <input
                    value={landlordPhone}
                    onChange={(e) => {
                        setLandlordPhone(e.target.value);
                    }}
                    placeholder="Landlord Phone"
                    className={`w-full p-2 rounded  "border-gray-300"}`}
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Reason For Leaving*</label>
                <input
                    value={reasonForLeaving}
                    onChange={(e) => {
                        setReasonForLeaving(e.target.value);
                    }}
                    placeholder="Reason for leaving"
                    className={`w-full p-2 rounded  "border-gray-300"}`}
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Amount of Rent*</label>
                <input
                    value={rent}
                    onChange={(e) => {
                        setRent(e.target.value);
                        setIsRentEmpty(false);
                    }}
                    placeholder="Rent"
                    className={`w-full p-2 rounded ${isRentEmpty? "border-red-500" : "border-gray-300"}`}
                />
                {isPresentHomeAddressEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please enter rent</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Is your present rent up to date?*</label>
                <select
                    value={isRentUpToDate}
                    onChange={(e) => {
                        setIsRentUpToDate(e.target.value);
                        setIsRentUpToDateEmpty(false);
                    }}
                    className={`w-full p-2 rounded ${isRentUpToDateEmpty ? "border-red-500" : "border-gray-300"}`}
                >
                    <option value="">Select an option</option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                {isRentUpToDateEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please select an option</div>
                )}
            </div>

            <button onClick={goBack} className="w-full py-2 px-4 mt-2 bg-gray-500 text-white rounded hover:bg-gray-600 transition-colors">Back</button>
            <button onClick={nextStep} className="w-full py-2 px-4 mt-4 bg-blue-500 text-white rounded hover:bg-blue-600 transition-colors">Next</button>
        </div>
    );
}

export default Step2;
