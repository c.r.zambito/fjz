import React, { useState } from 'react'

function Step3({ updateData, goBack, formData }) {
    const [proposedOccupantAmount, setProposedOccupantAmount] = useState(formData.proposedOccupantAmount || "");

    const [occupant1Name, setOccupant1Name] = useState(formData.occupant1Name || "");
    const [occupant1Relationship, setOccupant1Relationship] = useState(formData.occupant1Relationship || "");
    const [occupant1Occupation, setOccupant1Occupation] = useState(formData.occupant1Occupation || "");
    const [occupant1Age, setOccupant1Age] = useState(formData.occupant1Age || "");

    const [occupant2Name, setOccupant2Name] = useState(formData.occupant2Name || "");
    const [occupant2Relationship, setOccupant2Relationship] = useState(formData.occupant2Relationship || "");
    const [occupant2Occupation, setOccupant2Occupation] = useState(formData.occupant2Occupation || "");
    const [occupant2Age, setOccupant2Age] = useState(formData.occupant2Age || "");

    const [occupant3Name, setOccupant3Name] = useState(formData.occupant3Name || "");
    const [occupant3Relationship, setOccupant3Relationship] = useState(formData.occupant3Relationship || "");
    const [occupant3Occupation, setOccupant3Occupation] = useState(formData.occupant3Occupation || "");
    const [occupant3Age, setOccupant3Age] = useState(formData.occupant3Age || "");

    const [occupant4Name, setOccupant4Name] = useState(formData.occupant4Name || "");
    const [occupant4Relationship, setOccupant4Relationship] = useState(formData.occupant4Relationship || "");
    const [occupant4Occupation, setOccupant4Occupation] = useState(formData.occupant4Occupation || "");
    const [occupant4Age, setOccupant4Age] = useState(formData.occupant4Age || "");

    const [occupant5Name, setOccupant5Name] = useState(formData.occupant5Name || "");
    const [occupant5Relationship, setOccupant5Relationship] = useState(formData.occupant5Relationship || "");
    const [occupant5Occupation, setOccupant5Occupation] = useState(formData.occupant5Occupation || "");
    const [occupant5Age, setOccupant5Age] = useState(formData.occupant5Age || "");


    const [isProposedOccupantAmountEmpty, setIsProposedOccupantAmount] = useState(false);



    const nextStep = (event) => {
        event.preventDefault();

        // Check if required fields are empty
        if (!proposedOccupantAmount) {
            setIsProposedOccupantAmount(true);
        }


        if (proposedOccupantAmount ) {
            // Log the data to the console to verify it's being updated
            console.log(proposedOccupantAmount, occupant1Name, occupant2Name, occupant3Name, occupant4Name, occupant5Name, occupant1Relationship, occupant2Relationship, occupant3Relationship, occupant4Relationship, occupant5Relationship, occupant1Age, occupant2Age, occupant3Age, occupant4Age, occupant5Age, occupant1Occupation, occupant2Occupation, occupant3Occupation, occupant4Occupation, occupant5Occupation);

            // Then pass it back to the parent component:
            updateData(3, { proposedOccupantAmount, occupant1Name, occupant2Name, occupant3Name, occupant4Name, occupant5Name, occupant1Relationship, occupant2Relationship, occupant3Relationship, occupant4Relationship, occupant5Relationship, occupant1Age, occupant2Age, occupant3Age, occupant4Age, occupant5Age, occupant1Occupation, occupant2Occupation, occupant3Occupation, occupant4Occupation, occupant5Occupation});
        }
    };


    return (
        <div className="container mx-auto max-w-md p-6 bg-white rounded shadow-md">
            <h2 className="mb-6 text-2xl font-semibold text-gray-700">Additional Occupants</h2>


            <div className='flex items-center'>
                <div className='w-3/13 h-1 bg-blue-500'></div>
                <div className='w-10/13 h-1 bg-gray-300'></div>
            </div>
            <span className='text-gray-500'>Step 3 of 13</span>


            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700 pt-4">Number of Occupants*</label>
                <select
                    value={proposedOccupantAmount}
                    onChange={(e) => {
                        setProposedOccupantAmount(e.target.value);
                        setIsProposedOccupantAmount(false);
                    }}
                    className={`w-full p-2 rounded ${isProposedOccupantAmountEmpty ? "border-red-500" : "border-gray-300"}`}
                >
                    <option value="">Select an option</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5+</option>
                </select>
                {isProposedOccupantAmountEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please select an option</div>
                )}
            </div>


            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Occupant 1 Name</label>
                <input
                    value={occupant1Name}
                    onChange={(e) => {
                        setOccupant1Name(e.target.value);
                    }}
                    placeholder="Occupant 1 Name"
                    className={`w-full p-2 rounded  "border-gray-300"}`}
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Occupant 1 Relationship</label>
                <select
                    value={occupant1Relationship}
                    onChange={(e) => {
                        setOccupant1Relationship(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select a relationship</option>
                    <option value="spouse">Spouse</option>
                    <option value="child">Child</option>
                    <option value="parent">Parent</option>
                    <option value="sibling">Sibling</option>
                    <option value="other">Other</option>
                </select>
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Occupant 1 Occupation</label>
                <select
                    value={occupant1Occupation}
                    onChange={(e) => {
                        setOccupant1Occupation(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select an occupation</option>
                    <option value="employed">Employed</option>
                    <option value="unemployed">Unemployed</option>
                    <option value="student">Student</option>
                    <option value="retired">Retired</option>
                </select>
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Occupant 1 Age</label>
                <select
                    value={occupant1Age}
                    onChange={(e) => {
                        setOccupant1Age(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select an age range</option>
                    <option value="under18">Under 18</option>
                    <option value="18to25">18 to 25</option>
                    <option value="26to35">26 to 35</option>
                    <option value="36to45">36 to 45</option>
                    <option value="46to55">46 to 55</option>
                    <option value="56to65">56 to 65</option>
                    <option value="over65">Over 65</option>
                </select>
            </div>


            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Occupant 2 Name</label>
                <input
                    value={occupant2Name}
                    onChange={(e) => {
                        setOccupant2Name(e.target.value);
                    }}
                    placeholder="Occupant 2 Name"
                    className={`w-full p-2 rounded  "border-gray-300"}`}
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Occupant 2 Relationship</label>
                <select
                    value={occupant2Relationship}
                    onChange={(e) => {
                        setOccupant2Relationship(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select a relationship</option>
                    <option value="spouse">Spouse</option>
                    <option value="child">Child</option>
                    <option value="parent">Parent</option>
                    <option value="sibling">Sibling</option>
                    <option value="other">Other</option>
                </select>
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Occupant 2 Occupation</label>
                <select
                    value={occupant2Occupation}
                    onChange={(e) => {
                        setOccupant2Occupation(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select an occupation</option>
                    <option value="employed">Employed</option>
                    <option value="unemployed">Unemployed</option>
                    <option value="student">Student</option>
                    <option value="retired">Retired</option>
                </select>
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Occupant 2 Age</label>
                <select
                    value={occupant2Age}
                    onChange={(e) => {
                        setOccupant2Age(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select an age range</option>
                    <option value="under18">Under 18</option>
                    <option value="18to25">18 to 25</option>
                    <option value="26to35">26 to 35</option>
                    <option value="36to45">36 to 45</option>
                    <option value="46to55">46 to 55</option>
                    <option value="56to65">56 to 65</option>
                    <option value="over65">Over 65</option>
                </select>
            </div>


            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Occupant 3 Name</label>
                <input
                    value={occupant3Name}
                    onChange={(e) => {
                        setOccupant3Name(e.target.value);
                    }}
                    placeholder="Occupant 3 Name"
                    className={`w-full p-2 rounded  "border-gray-300"}`}
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Occupant 3 Relationship</label>
                <select
                    value={occupant3Relationship}
                    onChange={(e) => {
                        setOccupant3Relationship(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select a relationship</option>
                    <option value="spouse">Spouse</option>
                    <option value="child">Child</option>
                    <option value="parent">Parent</option>
                    <option value="sibling">Sibling</option>
                    <option value="other">Other</option>
                </select>
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Occupant 3 Occupation</label>
                <select
                    value={occupant3Occupation}
                    onChange={(e) => {
                        setOccupant3Occupation(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select an occupation</option>
                    <option value="employed">Employed</option>
                    <option value="unemployed">Unemployed</option>
                    <option value="student">Student</option>
                    <option value="retired">Retired</option>
                </select>
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Occupant 3 Age</label>
                <select
                    value={occupant3Age}
                    onChange={(e) => {
                        setOccupant3Age(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select an age range</option>
                    <option value="under18">Under 18</option>
                    <option value="18to25">18 to 25</option>
                    <option value="26to35">26 to 35</option>
                    <option value="36to45">36 to 45</option>
                    <option value="46to55">46 to 55</option>
                    <option value="56to65">56 to 65</option>
                    <option value="over65">Over 65</option>
                </select>
            </div>


            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Occupant 4 Name</label>
                <input
                    value={occupant4Name}
                    onChange={(e) => {
                        setOccupant4Name(e.target.value);
                    }}
                    placeholder="Occupant 4 Name"
                    className={`w-full p-2 rounded  "border-gray-300"}`}
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Occupant 4 Relationship</label>
                <select
                    value={occupant4Relationship}
                    onChange={(e) => {
                        setOccupant4Relationship(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select a relationship</option>
                    <option value="spouse">Spouse</option>
                    <option value="child">Child</option>
                    <option value="parent">Parent</option>
                    <option value="sibling">Sibling</option>
                    <option value="other">Other</option>
                </select>
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Occupant 4 Occupation</label>
                <select
                    value={occupant4Occupation}
                    onChange={(e) => {
                        setOccupant4Occupation(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select an occupation</option>
                    <option value="employed">Employed</option>
                    <option value="unemployed">Unemployed</option>
                    <option value="student">Student</option>
                    <option value="retired">Retired</option>
                </select>
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Occupant 4 Age</label>
                <select
                    value={occupant4Age}
                    onChange={(e) => {
                        setOccupant4Age(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select an age range</option>
                    <option value="under18">Under 18</option>
                    <option value="18to25">18 to 25</option>
                    <option value="26to35">26 to 35</option>
                    <option value="36to45">36 to 45</option>
                    <option value="46to55">46 to 55</option>
                    <option value="56to65">56 to 65</option>
                    <option value="over65">Over 65</option>
                </select>
            </div>


            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Occupant 5 Name</label>
                <input
                    value={occupant5Name}
                    onChange={(e) => {
                        setOccupant5Name(e.target.value);
                    }}
                    placeholder="Occupant 5 Name"
                    className={`w-full p-2 rounded  "border-gray-300"}`}
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Occupant 5 Relationship</label>
                <select
                    value={occupant5Relationship}
                    onChange={(e) => {
                        setOccupant5Relationship(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select a relationship</option>
                    <option value="spouse">Spouse</option>
                    <option value="child">Child</option>
                    <option value="parent">Parent</option>
                    <option value="sibling">Sibling</option>
                    <option value="other">Other</option>
                </select>
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Occupant 5 Occupation</label>
                <select
                    value={occupant5Occupation}
                    onChange={(e) => {
                        setOccupant5Occupation(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select an occupation</option>
                    <option value="employed">Employed</option>
                    <option value="unemployed">Unemployed</option>
                    <option value="student">Student</option>
                    <option value="retired">Retired</option>
                </select>
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Occupant 5 Age</label>
                <select
                    value={occupant5Age}
                    onChange={(e) => {
                        setOccupant5Age(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select an age range</option>
                    <option value="under18">Under 18</option>
                    <option value="18to25">18 to 25</option>
                    <option value="26to35">26 to 35</option>
                    <option value="36to45">36 to 45</option>
                    <option value="46to55">46 to 55</option>
                    <option value="56to65">56 to 65</option>
                    <option value="over65">Over 65</option>
                </select>
            </div>



            <button onClick={goBack} className="w-full py-2 px-4 mt-2 bg-gray-500 text-white rounded hover:bg-gray-600 transition-colors">Back</button>
            <button onClick={nextStep} className="w-full py-2 px-4 mt-4 bg-blue-500 text-white rounded hover:bg-blue-600 transition-colors">Next</button>
        </div>
    );
}

export default Step3
