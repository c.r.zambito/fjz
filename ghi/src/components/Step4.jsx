import React, { useState } from 'react'

function Step4({ updateData, goBack, formData }) {
    const [proposedNumberOfPets, setProposedNumberOfPets] = useState(formData.proposedNumberOfPets || "");

    const [proposedPetName1, setProposedPetName1] = useState(formData.proposedPetName1 || "");
    const [proposedPetBreed1, setProposedPetBreed1] = useState(formData.proposedPetBreed1 || "");
    const [proposedPetIndoorOutdoor1, setProposedPetIndoorOutdoor1] = useState(formData.proposedPetIndoorOutdoor1 || "");
    const [proposedPetAge1, setProposedPetAge1] = useState(formData.proposedPetAge1 || "");

    const [proposedPetName2, setProposedPetName2] = useState(formData.proposedPetName2 || "");
    const [proposedPetBreed2, setProposedPetBreed2] = useState(formData.proposedPetBreed2 || "");
    const [proposedPetIndoorOutdoor2, setProposedPetIndoorOutdoor2] = useState(formData.proposedPetIndoorOutdoor2 || "");
    const [proposedPetAge2, setProposedPetAge2] = useState(formData.proposedPetAge2 || "");

    const [proposedPetName3, setProposedPetName3] = useState(formData.proposedPetName3 || "");
    const [proposedPetBreed3, setProposedPetBreed3] = useState(formData.proposedPetBreed3 || "");
    const [proposedPetIndoorOutdoor3, setProposedPetIndoorOutdoor3] = useState(formData.proposedPetIndoorOutdoor3 || "");
    const [proposedPetAge3, setProposedPetAge3] = useState(formData.proposedPetAge3 || "");


    const [isProposedNumberOfPetsEmpty, setIsProposedNumberOfPetsEmpty] = useState(false);



    const nextStep = (event) => {
        event.preventDefault();

        // Check if required fields are empty
        if (proposedNumberOfPets.trim() === "") {
            setIsProposedNumberOfPetsEmpty(true);
        }


        if (proposedNumberOfPets ) {
            // Log the data to the console to verify it's being updated
            console.log(proposedNumberOfPets.trim() !== "");

            // Then pass it back to the parent component:
            updateData(4, { proposedNumberOfPets, proposedPetName1, proposedPetBreed1, proposedPetIndoorOutdoor1, proposedPetAge1, proposedPetName2, proposedPetBreed2, proposedPetIndoorOutdoor2, proposedPetAge2, proposedPetName3, proposedPetBreed3, proposedPetIndoorOutdoor3, proposedPetAge3});
        }
    };


    return (
        <div className="container mx-auto max-w-md p-6 bg-white rounded shadow-md">
            <h2 className="mb-6 text-2xl font-semibold text-gray-700">Proposed Pet(s)</h2>


            <div className='flex items-center'>
                <div className='w-4/13 h-1 bg-blue-500'></div>
                <div className='w-9/13 h-1 bg-gray-300'></div>
            </div>
            <span className='text-gray-500'>Step 4 of 13</span>



            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700 pt-4">Number of Pets*</label>
                <select
                    value={proposedNumberOfPets}
                    onChange={(e) => {
                        setProposedNumberOfPets(e.target.value);
                        setIsProposedNumberOfPetsEmpty(false);
                    }}
                    className={`w-full p-2 rounded ${isProposedNumberOfPetsEmpty ? "border-red-500" : "border-gray-300"}`}
                >
                    <option value="">Select an option</option>
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5+</option>
                </select>
                {isProposedNumberOfPetsEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please select an option</div>
                )}
            </div>


            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Pets Name (1)</label>
                <input
                    value={proposedPetName1}
                    onChange={(e) => {
                        setProposedPetName1(e.target.value);
                    }}
                    placeholder="Pets Name (1)"
                    className={`w-full p-2 rounded  "border-gray-300"}`}
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Pet Type</label>
                <select
                    value={proposedPetBreed1}
                    onChange={(e) => {
                        setProposedPetBreed1(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select a type</option>
                    <option value="Small Dog">Small Dog (under 10 LBS) 🐶</option>
                    <option value="Md Dog">Medium Dog  (Over 10 Under 35 LBS) 🐕</option>
                    <option value="Lg Dog">Large Dog (Over 35 LBS) 🐕‍🦺</option>
                    <option value="Pit bull">Pit Bull🐩</option>
                    <option value="Cat">Cat 🐈</option>
                    <option value="Bird">Bird 🦜</option>
                    <option value="Fish">Fish 🐠</option>
                    <option value="Rabbit">Rabbit 🐇</option>
                    <option value="Hamster">Hamster 🐹</option>
                    <option value="Guinea Pig">Guinea Pig 🐷</option>
                    <option value="Turtle">Turtle 🐢</option>
                    <option value="Snake">Snake 🐍</option>
                    <option value="Lizard">Lizard 🦎</option>
                    <option value="Horse">Horse 🐴</option>
                    <option value="Goat">Goat 🐐</option>
                    <option value="Reptile">Reptile 🦕</option>
                    <option value="Duck">Duck 🦆</option>
                    <option value="Amphibian">Amphibian 🐸</option>
                    <option value="Insect">Insect 🐞</option>
                    <option value="Rodent">Rodent 🐁</option>
                    <option value="other">Other 🦖🦁</option>
                </select>
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Pet 1 (Indoor or Outdoor)</label>
                <select
                    value={proposedPetIndoorOutdoor1}
                    onChange={(e) => {
                        setProposedPetIndoorOutdoor1(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select</option>
                    <option value="Indoor">Indoor</option>
                    <option value="Outdoor">Outdoor</option>
                    <option value="both">Indoor/Outdoor</option>
                </select>
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Pet 1 (Age)</label>
                <select
                    value={proposedPetAge1}
                    onChange={(e) => {
                        setProposedPetAge1(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10+">10+</option>
                </select>
            </div>


            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Pets Name (2)</label>
                <input
                    value={proposedPetName2}
                    onChange={(e) => {
                        setProposedPetName2(e.target.value);
                    }}
                    placeholder="Pets Name (2)"
                    className={`w-full p-2 rounded  "border-gray-300"}`}
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Pet Type (2)</label>
                <select
                    value={proposedPetBreed2}
                    onChange={(e) => {
                        setProposedPetBreed2(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select a type</option>
                    <option value="Small Dog">Small Dog (under 10 LBS) 🐶</option>
                    <option value="Md Dog">Medium Dog  (Over 10 Under 35 LBS) 🐕</option>
                    <option value="Lg Dog">Large Dog (Over 35 LBS) 🐕‍🦺</option>
                    <option value="Pit bull">Pit Bull🐩</option>
                    <option value="Cat">Cat 🐈</option>
                    <option value="Bird">Bird 🦜</option>
                    <option value="Fish">Fish 🐠</option>
                    <option value="Rabbit">Rabbit 🐇</option>
                    <option value="Hamster">Hamster 🐹</option>
                    <option value="Guinea Pig">Guinea Pig 🐷</option>
                    <option value="Turtle">Turtle 🐢</option>
                    <option value="Snake">Snake 🐍</option>
                    <option value="Lizard">Lizard 🦎</option>
                    <option value="Horse">Horse 🐴</option>
                    <option value="Goat">Goat 🐐</option>
                    <option value="Reptile">Reptile 🦕</option>
                    <option value="Duck">Duck 🦆</option>
                    <option value="Amphibian">Amphibian 🐸</option>
                    <option value="Insect">Insect 🐞</option>
                    <option value="Rodent">Rodent 🐁</option>
                    <option value="other">Other 🦖🦁</option>
                </select>
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Pet 2 (Indoor or Outdoor)</label>
                <select
                    value={proposedPetIndoorOutdoor2}
                    onChange={(e) => {
                        setProposedPetIndoorOutdoor2(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select</option>
                    <option value="Indoor">Indoor</option>
                    <option value="Outdoor">Outdoor</option>
                    <option value="both">Indoor/Outdoor</option>
                </select>
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Pet 2 (Age)</label>
                <select
                    value={proposedPetAge2}
                    onChange={(e) => {
                        setProposedPetAge2(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10+">10+</option>
                </select>
            </div>


            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Pets Name (3)</label>
                <input
                    value={proposedPetName3}
                    onChange={(e) => {
                        setProposedPetName3(e.target.value);
                    }}
                    placeholder="Pets Name (3)"
                    className={`w-full p-2 rounded  "border-gray-300"}`}
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Pet Type (3)</label>
                <select
                    value={proposedPetBreed3}
                    onChange={(e) => {
                        setProposedPetBreed3(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select a type</option>
                    <option value="Small Dog">Small Dog (under 10 LBS) 🐶</option>
                    <option value="Md Dog">Medium Dog  (Over 10 Under 35 LBS) 🐕</option>
                    <option value="Lg Dog">Large Dog (Over 35 LBS) 🐕‍🦺</option>
                    <option value="Pit bull">Pit Bull🐩</option>
                    <option value="Cat">Cat 🐈</option>
                    <option value="Bird">Bird 🦜</option>
                    <option value="Fish">Fish 🐠</option>
                    <option value="Rabbit">Rabbit 🐇</option>
                    <option value="Hamster">Hamster 🐹</option>
                    <option value="Guinea Pig">Guinea Pig 🐷</option>
                    <option value="Turtle">Turtle 🐢</option>
                    <option value="Snake">Snake 🐍</option>
                    <option value="Lizard">Lizard 🦎</option>
                    <option value="Horse">Horse 🐴</option>
                    <option value="Goat">Goat 🐐</option>
                    <option value="Reptile">Reptile 🦕</option>
                    <option value="Duck">Duck 🦆</option>
                    <option value="Amphibian">Amphibian 🐸</option>
                    <option value="Insect">Insect 🐞</option>
                    <option value="Rodent">Rodent 🐁</option>
                    <option value="other">Other 🦖🦁</option>
                </select>
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Pet 3 (Indoor or Outdoor)</label>
                <select
                    value={proposedPetIndoorOutdoor3}
                    onChange={(e) => {
                        setProposedPetIndoorOutdoor3(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select</option>
                    <option value="Indoor">Indoor</option>
                    <option value="Outdoor">Outdoor</option>
                    <option value="both">Indoor/Outdoor</option>
                </select>
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Pet 3 (Age)</label>
                <select
                    value={proposedPetAge3}
                    onChange={(e) => {
                        setProposedPetAge3(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10+">10+</option>
                </select>
            </div>


            <button onClick={goBack} className="w-full py-2 px-4 mt-2 bg-gray-500 text-white rounded hover:bg-gray-600 transition-colors">Back</button>
            <button onClick={nextStep} className="w-full py-2 px-4 mt-4 bg-blue-500 text-white rounded hover:bg-blue-600 transition-colors">Next</button>
        </div>
    );
}

export default Step4
