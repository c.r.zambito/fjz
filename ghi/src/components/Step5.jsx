import React, { useState } from 'react'

function Step5({ updateData, goBack, formData }) {
    const [vehicleAmount, setVehicleAmount] = useState(formData.vehicleAmount || "");

    const [vehicleYear1, setVehicleYear1] = useState(formData.vehicleYear1 || "");
    const [vehicleMake1, setVehicleMake1] = useState(formData.vehicleMake1 || "");
    const [vehicleModel1, setVehicleModel1] = useState(formData.vehicleModel1 || "");
    const [vehicleColor1, setVehicleColor1] = useState(formData.vehicleColor1 || "");
    const [vehiclePlateNumber1, setVehiclePlateNumber1] = useState(formData.vehiclePlateNumber1 || "");
    const [vehiclePlateState1, setVehiclePlateState1] = useState(formData.vehiclePlateState1 || "");

    const [vehicleYear2, setVehicleYear2] = useState(formData.vehicleYear2 || "");
    const [vehicleMake2, setVehicleMake2] = useState(formData.vehicleMake2 || "");
    const [vehicleModel2, setVehicleModel2] = useState(formData.vehicleModel2 || "");
    const [vehicleColor2, setVehicleColor2] = useState(formData.vehicleColor2 || "");
    const [vehiclePlateNumber2, setVehiclePlateNumber2] = useState(formData.vehiclePlateNumber2 || "");
    const [vehiclePlateState2, setVehiclePlateState2] = useState(formData.vehiclePlateState2 || "");



    const [isVehicleAmountEmpty, setIsVehicleAmountEmpty] = useState(false);



    const nextStep = (event) => {
        event.preventDefault();

        // Check if required fields are empty
        if (vehicleAmount.trim() === "") {
            setIsVehicleAmountEmpty(true);
        }


        if ( vehicleAmount ) {
            // Log the data to the console to verify it's being updated
            console.log(vehicleAmount.trim() !== "");

            // Then pass it back to the parent component:
            updateData(5, { vehicleAmount, vehicleYear1, vehicleMake1, vehicleModel1, vehicleColor1, vehiclePlateNumber1, vehiclePlateState1, vehicleYear2, vehicleMake2, vehicleModel2, vehicleColor2, vehiclePlateNumber2, vehiclePlateState2 });
        }
    };


    return (
        <div className="container mx-auto max-w-md p-6 bg-white rounded shadow-md">
            <h2 className="mb-6 text-2xl font-semibold text-gray-700">Vehicle Information</h2>


            <div className='flex items-center'>
                <div className='w-5/13 h-1 bg-blue-500'></div>
                <div className='w-8/13 h-w bg-gray-300'></div>
            </div>
            <span className='text-gray-500'>Step 5 of 13</span>


            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700 pt-4">Number of Vehicles*</label>
                <select
                    value={vehicleAmount}
                    onChange={(e) => {
                        setVehicleAmount(e.target.value);
                        setIsVehicleAmountEmpty(false);
                    }}
                    className={`w-full p-2 rounded ${isVehicleAmountEmpty ? "border-red-500" : "border-gray-300"}`}
                >
                    <option value="">Select an option</option>
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="2 or more">3 or more</option>
                </select>
                {isVehicleAmountEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please select an option</div>
                )}
            </div>


            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Vehicle Year (1)</label>
                <input
                    type="number"
                    value={vehicleYear1}
                    onChange={(e) => {
                        setVehicleYear1(e.target.value);
                    }}
                    placeholder="Vehicle Year (1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Vehicle Make (1)</label>
                <input
                    type="text"
                    value={vehicleMake1}
                    onChange={(e) => {
                        setVehicleMake1(e.target.value);
                    }}
                    placeholder="Vehicle Make (1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Vehicle Model (1)</label>
                <input
                    type="text"
                    value={vehicleModel1}
                    onChange={(e) => {
                        setVehicleModel1(e.target.value);
                    }}
                    placeholder="Vehicle Model (1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Vehicle Color (1)</label>
                <input
                    type="text"
                    value={vehicleColor1}
                    onChange={(e) => {
                        setVehicleColor1(e.target.value);
                    }}
                    placeholder="Vehicle Color (1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Vehicle Plate Number (1)</label>
                <input
                    type="text"
                    value={vehiclePlateNumber1}
                    onChange={(e) => {
                        setVehiclePlateNumber1(e.target.value);
                    }}
                    placeholder="Vehicle Plate Number (1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Vehicle Plate State (1)</label>
                <input
                    type="text"
                    value={vehiclePlateState1}
                    onChange={(e) => {
                        setVehiclePlateState1(e.target.value);
                    }}
                    placeholder="Vehicle Plate State (1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>


            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Vehicle Year (2)</label>
                <input
                    type="number"
                    value={vehicleYear2}
                    onChange={(e) => {
                        setVehicleYear2(e.target.value);
                    }}
                    placeholder="Vehicle Year (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Vehicle Make (2)</label>
                <input
                    type="text"
                    value={vehicleMake2}
                    onChange={(e) => {
                        setVehicleMake2(e.target.value);
                    }}
                    placeholder="Vehicle Make (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Vehicle Model (2)</label>
                <input
                    type="text"
                    value={vehicleModel2}
                    onChange={(e) => {
                        setVehicleModel2(e.target.value);
                    }}
                    placeholder="Vehicle Model (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Vehicle Color (2)</label>
                <input
                    type="text"
                    value={vehicleColor2}
                    onChange={(e) => {
                        setVehicleColor2(e.target.value);
                    }}
                    placeholder="Vehicle Color (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Vehicle Plate Number (2)</label>
                <input
                    type="text"
                    value={vehiclePlateNumber2}
                    onChange={(e) => {
                        setVehiclePlateNumber2(e.target.value);
                    }}
                    placeholder="Vehicle Plate Number (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Vehicle Plate State (2)</label>
                <input
                    type="text"
                    value={vehiclePlateState2}
                    onChange={(e) => {
                        setVehiclePlateState2(e.target.value);
                    }}
                    placeholder="Vehicle Plate State (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>




            <button onClick={goBack} className="w-full py-2 px-4 mt-2 bg-gray-500 text-white rounded hover:bg-gray-600 transition-colors">Back</button>
            <button onClick={nextStep} className="w-full py-2 px-4 mt-4 bg-blue-500 text-white rounded hover:bg-blue-600 transition-colors">Next</button>
        </div>
    );
}

export default Step5
