import React, { useState } from 'react'

function Step6({ updateData, goBack, formData }) {
    const [employmentStatus, setEmploymentStatus] = useState(formData.employmentStatus || "");

    const [employmentCurrentEmployer1, setEmploymentCurrentEmployer1] = useState(formData.employmentCurrentEmployer1 || "");
    const [employmentOccupation1, setEmploymentOccupation1] = useState(formData.employmentOccupation1 || "");
    const [employmentHoursWeek1, setEmploymentHoursWeek1] = useState(formData.employmentHoursWeek1 || "");
    const [employmentSupervisor1, setEmploymentSupervisor1] = useState(formData.employmentSupervisor1 || "");
    const [employmentPhone1, setEmploymentPhone1] = useState(formData.employmentPhone1 || "");
    const [employmentYearsEmployed1, setEmploymentYearsEmployed1] = useState(formData.employmentYearsEmployed1 || "");
    const [employmentAddress1, setEmploymentAddress1] = useState(formData.employmentAddress1 || "");

    const [employmentCurrentEmployer2, setEmploymentCurrentEmployer2] = useState(formData.employmentCurrentEmployer2 || "");
    const [employmentOccupation2, setEmploymentOccupation2] = useState(formData.employmentOccupation2 || "");
    const [employmentHoursWeek2, setEmploymentHoursWeek2] = useState(formData.employmentHoursWeek2 || "");
    const [employmentSupervisor2, setEmploymentSupervisor2] = useState(formData.employmentSupervisor2 || "");
    const [employmentPhone2, setEmploymentPhone2] = useState(formData.employmentPhone2 || "");
    const [employmentYearsEmployed2, setEmploymentYearsEmployed2] = useState(formData.employmentYearsEmployed2 || "");
    const [employmentAddress2, setEmploymentAddress2] = useState(formData.employmentAddress2 || "");

    const [isEmploymentStatusEmpty, setIsEmploymentStatusEmpty] = useState(false);



    const nextStep = (event) => {
        event.preventDefault();

        // Check if required fields are empty
        if (employmentStatus.trim() === "") {
            setIsEmploymentStatusEmpty(true);
        }


        if ( employmentStatus ) {
            // Log the data to the console to verify it's being updated
            console.log(employmentStatus.trim() !== "");

            // Then pass it back to the parent component:
            updateData(6, { employmentStatus, employmentCurrentEmployer1, employmentOccupation1, employmentHoursWeek1, employmentSupervisor1, employmentPhone1, employmentYearsEmployed1, employmentAddress1, employmentCurrentEmployer2, employmentOccupation2, employmentHoursWeek2, employmentSupervisor2, employmentPhone2, employmentYearsEmployed2, employmentAddress2 });
        }
    };


    return (
        <div className="container mx-auto max-w-md p-6 bg-white rounded shadow-md">
            <h2 className="mb-6 text-2xl font-semibold text-gray-700">Employment</h2>


            <div className='flex items-center'>
                <div className='w-6/13 h-1 bg-blue-500'></div>
                <div className='w-7/13 h-1 bg-gray-300'></div>
            </div>
            <span className='text-gray-500'>Step 6 of 13</span>


            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700 pt-4">Employment Status*</label>
                <select
                    value={employmentStatus}
                    onChange={(e) => {
                        setEmploymentStatus(e.target.value);
                        setIsEmploymentStatusEmpty(false);
                    }}
                    className={`w-full p-2 rounded ${isEmploymentStatusEmpty ? "border-red-500" : "border-gray-300"}`}
                >
                    <option value="">Select an option</option>
                    <option value="Employed">Employed</option>
                    <option value="Unemployed">Unemployed</option>
                    <option value="Student">Student</option>
                    <option value="Retired">Retired</option>
                    <option value="Other">Other</option>
                </select>
                {isEmploymentStatusEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please select an option</div>
                )}
            </div>


            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Current Employer (1)</label>
                <input
                    type="text"
                    value={employmentCurrentEmployer1}
                    onChange={(e) => {
                        setEmploymentCurrentEmployer1(e.target.value);
                    }}
                    placeholder="Current Employer (1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Occupation (1)</label>
                <input
                    type="text"
                    value={employmentOccupation1}
                    onChange={(e) => {
                        setEmploymentOccupation1(e.target.value);
                    }}
                    placeholder="Occupation (1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Supervisor (1)</label>
                <input
                    type="text"
                    value={employmentSupervisor1}
                    onChange={(e) => {
                        setEmploymentSupervisor1(e.target.value);
                    }}
                    placeholder="Supervisor (1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Hours/Week (1)</label>
                <input
                    type="text"
                    value={employmentHoursWeek1}
                    onChange={(e) => {
                        setEmploymentHoursWeek1(e.target.value);
                    }}
                    placeholder="Hours/Week (1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Employer Phone (1)</label>
                <input
                    type="text"
                    value={employmentPhone1}
                    onChange={(e) => {
                        setEmploymentPhone1(e.target.value);
                    }}
                    placeholder="Employment Phone (1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Years Employed (1)</label>
                <input
                    type="text"
                    value={employmentYearsEmployed1}
                    onChange={(e) => {
                        setEmploymentYearsEmployed1(e.target.value);
                    }}
                    placeholder="Years Employed (1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Employment Address (1)</label>
                <input
                    type="text"
                    value={employmentAddress1}
                    onChange={(e) => {
                        setEmploymentAddress1(e.target.value);
                    }}
                    placeholder="Employment Address (1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>



            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Current Employer (2)</label>
                <input
                    type="text"
                    value={employmentCurrentEmployer2}
                    onChange={(e) => {
                        setEmploymentCurrentEmployer2(e.target.value);
                    }}
                    placeholder="Current Employer (1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Occupation (2)</label>
                <input
                    type="text"
                    value={employmentOccupation2}
                    onChange={(e) => {
                        setEmploymentOccupation2(e.target.value);
                    }}
                    placeholder="Occupation (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Supervisor (2)</label>
                <input
                    type="text"
                    value={employmentSupervisor2}
                    onChange={(e) => {
                        setEmploymentSupervisor2(e.target.value);
                    }}
                    placeholder="Supervisor (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Hours/Week (2)</label>
                <input
                    type="text"
                    value={employmentHoursWeek2}
                    onChange={(e) => {
                        setEmploymentHoursWeek2(e.target.value);
                    }}
                    placeholder="Hours/Week (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Employer Phone (2)</label>
                <input
                    type="text"
                    value={employmentPhone2}
                    onChange={(e) => {
                        setEmploymentPhone2(e.target.value);
                    }}
                    placeholder="Employment Phone (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Years Employed (2)</label>
                <input
                    type="text"
                    value={employmentYearsEmployed2}
                    onChange={(e) => {
                        setEmploymentYearsEmployed2(e.target.value);
                    }}
                    placeholder="Years Employed (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Employment Address (2)</label>
                <input
                    type="text"
                    value={employmentAddress2}
                    onChange={(e) => {
                        setEmploymentAddress2(e.target.value);
                    }}
                    placeholder="Employment Address (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>


            <button onClick={goBack} className="w-full py-2 px-4 mt-2 bg-gray-500 text-white rounded hover:bg-gray-600 transition-colors">Back</button>
            <button onClick={nextStep} className="w-full py-2 px-4 mt-4 bg-blue-500 text-white rounded hover:bg-blue-600 transition-colors">Next</button>
        </div>
    );
}

export default Step6
