import React, { useState } from 'react'

function Step7({ updateData, goBack, formData }) {
    const [currentIncome1, setCurrentIncome1] = useState(formData.currentIncome1 || "");
    const [currentIncomeAmount1, setCurrentIncomeAmount1] = useState(formData.currentIncomeAmount1 || "");
    const [currentIncomeSource1, setCurrentIncomeSource1] = useState(formData.currentIncomeSource1 || "");
    const [currentProofOfIncome1, setCurrentProofOfIncome1] = useState(formData.currentProofOfIncome1 || "");

    const [currentIncome2, setCurrentIncome2] = useState(formData.currentIncome2 || "");
    const [currentIncomeAmount2, setCurrentIncomeAmount2] = useState(formData.currentIncomeAmount2 || "");
    const [currentIncomeSource2, setCurrentIncomeSource2] = useState(formData.currentIncomeSource2 || "");
    const [currentProofOfIncome2, setCurrentProofOfIncome2] = useState(formData.currentProofOfIncome2 || "");


    const [isCurrentIncomeEmpty1, setIsCurrentIncomeEmpty1] = useState(false);
    const [isCurrentIncomeAmountEmpty1, setIsCurrentIncomeAmountEmpty1] = useState(false);
    const [isCurrentIncomeSourceEmpty1, setIsCurrentIncomeSourceEmpty1] = useState(false);
    const [isCurrentProofOfIncomeEmpty1, setIsCurrentProofOfIncomeEmpty1] = useState(false);



    const nextStep = (event) => {
        event.preventDefault();

        // Check if required fields are empty
        if (currentIncome1.trim() === "") {
            setIsCurrentIncomeEmpty1(true);
        }
        if (currentIncomeAmount1.trim() === "") {
            setIsCurrentIncomeAmountEmpty1(true);
        }
        if (currentIncomeSource1.trim() === "") {
            setIsCurrentIncomeSourceEmpty1(true);
        }
        if (currentProofOfIncome1.trim() === "") {
            setIsCurrentProofOfIncomeEmpty1(true);
        }


        if ( currentIncome1 ) {
            // Log the data to the console to verify it's being updated
            console.log(currentIncome1.trim() !== "");

            // Then pass it back to the parent component:
            updateData(7, { currentIncome1, currentIncomeAmount1, currentIncomeSource1, currentProofOfIncome1, currentIncome2, currentIncomeAmount2, currentIncomeSource2, currentProofOfIncome2 });
        }
    };


    return (
        <div className="container mx-auto max-w-md p-6 bg-white rounded shadow-md">
            <h2 className="mb-6 text-2xl font-semibold text-gray-700">Income</h2>


            <div className='flex items-center'>
                <div className='w-7/13 h-1 bg-blue-500'></div>
                <div className='w-6/13 h-1 bg-gray-300'></div>
            </div>
            <span className='text-gray-500'>Step 7 of 13</span>


            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700 pt-4">Current Income (1)*</label>
                <select
                    value={currentIncome1}
                    onChange={(e) => {
                        setCurrentIncome1(e.target.value);
                        setIsCurrentIncomeEmpty1(false);
                    }}
                    className={`w-full p-2 rounded ${isCurrentIncomeAmountEmpty1 ? "border-red-500" : "border-gray-300"}`}
                >
                    <option value="">Select an option</option>
                    <option value="Weekly">Weekly</option>
                    <option value="Biweekly">Biweekly</option>
                    <option value="Monthly">Monthly</option>
                    <option value="Yearly">Yearly</option>
                    <option value="Other">Other</option>
                </select>
                {isCurrentIncomeEmpty1 && (
                    <div className="text-red-500 text-xs mt-1">Please select an option</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Current Income Amount (1)*</label>
                <input
                    value={currentIncomeAmount1}
                    onChange={(e) => {
                        setCurrentIncomeAmount1(e.target.value);
                        setIsCurrentIncomeAmountEmpty1(false);
                    }}
                    placeholder="Current Income Amount*"
                    className={`w-full p-2 rounded ${isCurrentIncomeAmountEmpty1 ? "border-red-500" : "border-gray-300"}`}
                />
                {isCurrentIncomeAmountEmpty1 && (
                    <div className="text-red-500 text-xs mt-1">Please enter your current income amount</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Income Source (1)*</label>
                <input
                    value={currentIncomeSource1}
                    onChange={(e) => {
                        setCurrentIncomeSource1(e.target.value);
                        setIsCurrentIncomeSourceEmpty1(false);
                    }}
                    placeholder="Income Source*"
                    className={`w-full p-2 rounded ${isCurrentIncomeSourceEmpty1 ? "border-red-500" : "border-gray-300"}`}
                />
                {isCurrentIncomeSourceEmpty1 && (
                    <div className="text-red-500 text-xs mt-1">Please enter your income source</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Proof of Income*</label>
                <select
                    value={currentProofOfIncome1}
                    onChange={(e) => {
                        setCurrentProofOfIncome1(e.target.value);
                        setIsCurrentProofOfIncomeEmpty1(false);
                    }}
                    className={`w-full p-2 rounded ${isCurrentProofOfIncomeEmpty1 ? "border-red-500" : "border-gray-300"}`}
                >
                    <option value="">--Please choose an option--</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
                {isCurrentProofOfIncomeEmpty1 && (
                    <div className="text-red-500 text-xs mt-1">Please select your proof of income</div>
                )}
            </div>



            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Current Income (2)</label>
                <select
                    value={currentIncome2}
                    onChange={(e) => {
                        setCurrentIncome2(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select an option</option>
                    <option value="Weekly">Weekly</option>
                    <option value="Biweekly">Biweekly</option>
                    <option value="Monthly">Monthly</option>
                    <option value="Yearly">Yearly</option>
                    <option value="Other">Other</option>
                </select>
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Current Income Amount (2)</label>
                <input
                    type="text"
                    value={currentIncomeAmount2}
                    onChange={(e) => {
                        setCurrentIncomeAmount2(e.target.value);
                    }}
                    placeholder="Current Income Amount (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Current Income Source (2)</label>
                <input
                    type="text"
                    value={currentIncomeSource2}
                    onChange={(e) => {
                        setCurrentIncomeSource2(e.target.value);
                    }}
                    placeholder="Current Income Source (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Proof of Income (2)</label>
                <select
                    value={currentProofOfIncome2}
                    onChange={(e) => {
                        setCurrentProofOfIncome2(e.target.value);
                    }}
                    className="w-full p-2 rounded border-gray-300"
                >
                    <option value="">Select an option</option>
                    <option value="Yes">Yes</option>
                    <option value="No">No</option>
                </select>
            </div>





            <button onClick={goBack} className="w-full py-2 px-4 mt-2 bg-gray-500 text-white rounded hover:bg-gray-600 transition-colors">Back</button>
            <button onClick={nextStep} className="w-full py-2 px-4 mt-4 bg-blue-500 text-white rounded hover:bg-blue-600 transition-colors">Next</button>
        </div>
    );
}

export default Step7
