import React, { useState } from 'react'

function Step8({ updateData, goBack, formData }) {
    const [carLoan, setCarLoan] = useState(formData.carLoan || "");
    const [carLoanBalanceOwed, setCarLoanBalanceOwed] = useState(formData.carLoanBalanceOwed || "");
    const [carLoanMonthlyPayment, setCarLoanMonthlyPayment] = useState(formData.carLoanMonthlyPayment || "");
    const [carLoanCreditorPhone, setCarLoanCreditorPhone] = useState(formData.carLoanCreditorPhone || "");

    const [creditCardCompany1, setCreditCardCompany1] = useState(formData.creditCardCompany1 || "");
    const [creditCardCompanyBalanceOwed1, setCreditCardCompanyBalanceOwed1] = useState(formData.creditCardCompanyBalanceOwed1 || "");
    const [creditCardCompanyMonthlyPayment1, setCreditCardCompanyMonthlyPayment1] = useState(formData.creditCardCompanyMonthlyPayment1 || "");
    const [creditCardCompanyCreditorPhone1, setCreditCardCompanyCreditorPhone1] = useState(formData.creditCardCompanyCreditorPhone1 || "");

    const [creditCardCompany2, setCreditCardCompany2] = useState(formData.creditCardCompany2 || "");
    const [creditCardCompanyBalanceOwed2, setCreditCardCompanyBalanceOwed2] = useState(formData.creditCardCompanyBalanceOwed2 || "");
    const [creditCardCompanyMonthlyPayment2, setCreditCardCompanyMonthlyPayment2] = useState(formData.creditCardCompanyMonthlyPayment2 || "");
    const [creditCardCompanyCreditorPhone2, setCreditCardCompanyCreditorPhone2] = useState(formData.creditCardCompanyCreditorPhone2 || "");

    const [creditCardCompany3, setCreditCardCompany3] = useState(formData.creditCardCompany3 || "");
    const [creditCardCompanyBalanceOwed3, setCreditCardCompanyBalanceOwed3] = useState(formData.creditCardCompanyBalanceOwed3 || "");
    const [creditCardCompanyMonthlyPayment3, setCreditCardCompanyMonthlyPayment3] = useState(formData.creditCardCompanyMonthlyPayment3 || "");
    const [creditCardCompanyCreditorPhone3, setCreditCardCompanyCreditorPhone3] = useState(formData.creditCardCompanyCreditorPhone3 || "");

    const [childSupportOther, setChildSupportOther] = useState(formData.childSupportOther || "");
    const [childSupportOtherBalance, setChildSupportOtherBalance] = useState(formData.childSupportOtherBalance || "");
    const [childSupportOtherMonthlyPayment, setChildSupportOtherMonthlyPayment] = useState(formData.childSupportOtherMonthlyPayment || "");
    const [childSupportOtherCreditorPhone, setChildSupportOtherCreditorPhone] = useState(formData.childSupportOtherCreditorPhone || "");

    const [bankAccountName, setBankAccountName] = useState(formData.bankAccountName || "");
    const [bankAccountBalance, setBankAccountBalance] = useState(formData.bankAccountBalance || "");
    const [bankAccountMonthlyPayment, setBankAccountMonthlyPayment] = useState(formData.bankAccountMonthlyPayment || "");
    const [bankAccountNumber, setBankAccountNumber] = useState(formData.bankAccountNumber || "");


    const [isCarLoanEmpty, setIsCarLoanEmpty] = useState(false);




    const nextStep = (event) => {
        event.preventDefault();

        // Check if required fields are empty
        if (carLoan.trim() === "") {
            setIsCarLoanEmpty(true);
        }



        if ( carLoan ) {
            // Log the data to the console to verify it's being updated
            console.log(carLoan.trim() !== "");

            // Then pass it back to the parent component:
            updateData(8, { carLoan, carLoanBalanceOwed, carLoanMonthlyPayment, carLoanCreditorPhone, creditCardCompany1, creditCardCompanyBalanceOwed1, creditCardCompanyMonthlyPayment1, creditCardCompanyCreditorPhone1, creditCardCompany2, creditCardCompanyBalanceOwed2, creditCardCompanyMonthlyPayment2, creditCardCompanyCreditorPhone2, creditCardCompany3, creditCardCompanyBalanceOwed3, creditCardCompanyMonthlyPayment3, creditCardCompanyCreditorPhone3, childSupportOther, childSupportOtherBalance, childSupportOtherMonthlyPayment, childSupportOtherCreditorPhone, bankAccountName, bankAccountBalance, bankAccountMonthlyPayment, bankAccountNumber });
        }
    };


    return (
        <div className="container mx-auto max-w-md p-6 bg-white rounded shadow-md">
            <h2 className="mb-6 text-2xl font-semibold text-gray-700">Credit Card / Financial Information</h2>


            <div className='flex items center'>
                <div className='w-8/13 h-1 bg-blue-500'></div>
                <div className='w-5/13 h-1 bg-gray-300'></div>
            </div>
            <span className='text-gray-500'>Step 8 of 13</span>


            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700 pt-4">Car Loan Lien Holder</label>
                <input
                    value={carLoan}
                    onChange={(e) => {
                        setCarLoan(e.target.value);
                        setIsCarLoanEmpty(false);
                    }}
                    placeholder="Car Loan Lien Holder"
                    className={`w-full p-2 rounded ${isCarLoanEmpty ? "border-red-500" : "border-gray-300"}`}
                />
                {isCarLoanEmpty && (
                    <div className="text-red-500 text-xs mt-1">Please enter your car loan lien holder or N/A if none</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Car Loan Balance</label>
                <input
                    type="text"
                    value={carLoanBalanceOwed}
                    onChange={(e) => {
                        setCarLoanBalanceOwed(e.target.value);
                    }}
                    placeholder="Car Loan Balance"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Car Loan Monthly Payment</label>
                <input
                    type="text"
                    value={carLoanMonthlyPayment}
                    onChange={(e) => {
                        setCarLoanMonthlyPayment(e.target.value);
                    }}
                    placeholder="Car Loan Monthly Payment"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Car Loan Creditor Phone Number</label>
                <input
                    type="text"
                    value={carLoanCreditorPhone}
                    onChange={(e) => {
                        setCarLoanCreditorPhone(e.target.value);
                    }}
                    placeholder="Car Loan Creditor Phone Number"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>



            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Credit Card Company (1)</label>
                <input
                    type="text"
                    value={creditCardCompany1}
                    onChange={(e) => {
                        setCreditCardCompany1(e.target.value);
                    }}
                    placeholder="Credit Card Company (1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Credit Card Balanced Owed (1)</label>
                <input
                    type="text"
                    value={creditCardCompanyBalanceOwed1}
                    onChange={(e) => {
                        setCreditCardCompanyBalanceOwed1(e.target.value);
                    }}
                    placeholder="Credit Card Balanced Owed (1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Credit Card Monthly Payment (1)</label>
                <input
                    type="text"
                    value={creditCardCompanyMonthlyPayment1}
                    onChange={(e) => {
                        setCreditCardCompanyMonthlyPayment1(e.target.value);
                    }}
                    placeholder="Credit Card Monthly Payment (1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Creditor Phone Number (1)</label>
                <input
                    type="text"
                    value={creditCardCompanyCreditorPhone1}
                    onChange={(e) => {
                        setCreditCardCompanyCreditorPhone1(e.target.value);
                    }}
                    placeholder="Creditor Phone Number (1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>



            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Credit Card Company (2)</label>
                <input
                    type="text"
                    value={creditCardCompany2}
                    onChange={(e) => {
                        setCreditCardCompany2(e.target.value);
                    }}
                    placeholder="Credit Card Company (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Credit Card Balanced Owed (2)</label>
                <input
                    type="text"
                    value={creditCardCompanyBalanceOwed2}
                    onChange={(e) => {
                        setCreditCardCompanyBalanceOwed2(e.target.value);
                    }}
                    placeholder="Credit Card Balanced Owed (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Credit Card Monthly Payment (2)</label>
                <input
                    type="text"
                    value={creditCardCompanyMonthlyPayment2}
                    onChange={(e) => {
                        setCreditCardCompanyMonthlyPayment2(e.target.value);
                    }}
                    placeholder="Credit Card Monthly Payment (1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Creditor Phone Number (2)</label>
                <input
                    type="text"
                    value={creditCardCompanyCreditorPhone2}
                    onChange={(e) => {
                        setCreditCardCompanyCreditorPhone2(e.target.value);
                    }}
                    placeholder="Creditor Phone Number (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>



            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Credit Card Company (3)</label>
                <input
                    type="text"
                    value={creditCardCompany3}
                    onChange={(e) => {
                        setCreditCardCompany3(e.target.value);
                    }}
                    placeholder="Credit Card Company (3)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Credit Card Balanced Owed (3)</label>
                <input
                    type="text"
                    value={creditCardCompanyBalanceOwed3}
                    onChange={(e) => {
                        setCreditCardCompanyBalanceOwed3(e.target.value);
                    }}
                    placeholder="Credit Card Balanced Owed (3)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Credit Card Monthly Payment (3)</label>
                <input
                    type="text"
                    value={creditCardCompanyMonthlyPayment3}
                    onChange={(e) => {
                        setCreditCardCompanyMonthlyPayment3(e.target.value);
                    }}
                    placeholder="Credit Card Monthly Payment (3)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Creditor Phone Number (3)</label>
                <input
                    type="text"
                    value={creditCardCompanyCreditorPhone3}
                    onChange={(e) => {
                        setCreditCardCompanyCreditorPhone3(e.target.value);
                    }}
                    placeholder="Creditor Phone Number (3)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>



            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Child Support / Other Creditor Owed</label>
                <input
                    type="text"
                    value={childSupportOther}
                    onChange={(e) => {
                        setChildSupportOther(e.target.value);
                    }}
                    placeholder="Child Support / Other Creditor Owed"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Child Support / Other (Balance)</label>
                <input
                    type="text"
                    value={childSupportOtherBalance}
                    onChange={(e) => {
                        setChildSupportOtherBalance(e.target.value);
                    }}
                    placeholder="Child Support / Other (Balance)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Child Support / Other (Monthly Payment)</label>
                <input
                    type="text"
                    value={childSupportOtherMonthlyPayment}
                    onChange={(e) => {
                        setChildSupportOtherMonthlyPayment(e.target.value);
                    }}
                    placeholder="Child Support / Other (Monthly Payment)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Child Support / Other (Creditor Phone)</label>
                <input
                    type="text"
                    value={childSupportOtherCreditorPhone}
                    onChange={(e) => {
                        setChildSupportOtherCreditorPhone(e.target.value);
                    }}
                    placeholder="Child Support / Other (Creditor Phone)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>



            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Bank Account Name Of Bank</label>
                <input
                    type="text"
                    value={bankAccountName}
                    onChange={(e) => {
                        setBankAccountName(e.target.value);
                    }}
                    placeholder="Bank Account Name Of Bank"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Bank Account Balance</label>
                <input
                    type="text"
                    value={bankAccountBalance}
                    onChange={(e) => {
                        setBankAccountBalance(e.target.value);
                    }}
                    placeholder="Bank Account Balance"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Bank Account Monthly Payment</label>
                <input
                    type="text"
                    value={bankAccountMonthlyPayment}
                    onChange={(e) => {
                        setBankAccountMonthlyPayment(e.target.value);
                    }}
                    placeholder="Bank Account Monthly Payment"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Account Number</label>
                <input
                    type="text"
                    value={bankAccountNumber}
                    onChange={(e) => {
                        setBankAccountNumber(e.target.value);
                    }}
                    placeholder="Account Number"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>





            <button onClick={goBack} className="w-full py-2 px-4 mt-2 bg-gray-500 text-white rounded hover:bg-gray-600 transition-colors">Back</button>
            <button onClick={nextStep} className="w-full py-2 px-4 mt-4 bg-blue-500 text-white rounded hover:bg-blue-600 transition-colors">Next</button>
        </div>
    );
}

export default Step8
