import React, { useState } from 'react'

function Step9({ updateData, goBack, formData }) {
    const [emergencyContact1, setEmergencyContact1] = useState(formData.emergencyContact1 || "");
    const [emergencyContactPhone1, setEmergencyContactPhone1] = useState(formData.emergencyContactPhone1 || "");
    const [emergencyContactRelationship1, setEmergencyContactRelationship1] = useState(formData.emergencyContactRelationship1 || "");
    const [emergencyContactAddress1, setEmergencyContactAddress1] = useState(formData.emergencyContactAddress1 || "");

    const [emergencyContact2, setEmergencyContact2] = useState(formData.emergencyContact2 || "");
    const [emergencyContactPhone2, setEmergencyContactPhone2] = useState(formData.emergencyContactPhone2 || "");
    const [emergencyContactRelationship2, setEmergencyContactRelationship2] = useState(formData.emergencyContactRelationship2 || "");
    const [emergencyContactAddress2, setEmergencyContactAddress2] = useState(formData.emergencyContactAddress2 || "");


    const [isEmergencyContactEmpty1, setIsEmergencyContactEmpty1] = useState(false);




    const nextStep = (event) => {
        event.preventDefault();

        // Check if required fields are empty
        if (emergencyContact1.trim() === "") {
            setIsEmergencyContactEmpty1(true);
        }



        if ( emergencyContact1 ) {
            // Log the data to the console to verify it's being updated
            console.log(emergencyContact1.trim() !== "");

            // Then pass it back to the parent component:
            updateData(9, { emergencyContact1, emergencyContactPhone1, emergencyContactRelationship1, emergencyContactAddress1, emergencyContact2, emergencyContactPhone2, emergencyContactRelationship2, emergencyContactAddress2 });
        }
    };


    return (
        <div className="container mx-auto max-w-md p-6 bg-white rounded shadow-md">
            <h2 className="mb-6 text-2xl font-semibold text-gray-700">Emergency Contact</h2>


            <div className="flex items-center">
                <div className="w-9/13 h-1 bg-blue-500"></div>
                <div className="w-4/13 h-1 bg-gray-300"></div>
            </div>
            <span className="text-gray-500">Step 9 of 13</span>


            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700 pt-4">Emergency Contact (1)*</label>
                <input
                    value={emergencyContact1}
                    onChange={(e) => {
                        setEmergencyContact1(e.target.value);
                        setIsEmergencyContactEmpty1(false);
                    }}
                    placeholder="Emergency Contact (1)"
                    className={`w-full p-2 rounded ${isEmergencyContactEmpty1 ? "border-red-500" : "border-gray-300"}`}
                />
                {isEmergencyContactEmpty1 && (
                    <div className="text-red-500 text-xs mt-1">Please enter your emergency contact</div>
                )}
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Emergency Contact Phone (1)</label>
                <input
                    type="text"
                    value={emergencyContactPhone1}
                    onChange={(e) => {
                        setEmergencyContactPhone1(e.target.value);
                    }}
                    placeholder="Emergency Contact Phone (1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Emergency Contact Relationship (1)</label>
                <input
                    type="text"
                    value={emergencyContactRelationship1}
                    onChange={(e) => {
                        setEmergencyContactRelationship1(e.target.value);
                    }}
                    placeholder="Emergency Contact Relationship (1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Emergency Contact Address(1)</label>
                <input
                    type="text"
                    value={emergencyContactAddress1}
                    onChange={(e) => {
                        setEmergencyContactAddress1(e.target.value);
                    }}
                    placeholder="Emergency Contact Address(1)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>


            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Emergency Contact (2)</label>
                <input
                    type="text"
                    value={emergencyContact2}
                    onChange={(e) => {
                        setEmergencyContact2(e.target.value);
                    }}
                    placeholder="Emergency Contact (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Emergency Contact Phone (2)</label>
                <input
                    type="text"
                    value={emergencyContactPhone2}
                    onChange={(e) => {
                        setEmergencyContactPhone2(e.target.value);
                    }}
                    placeholder="Emergency Contact Phone (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Emergency Contact Relationship (2)</label>
                <input
                    type="text"
                    value={emergencyContactRelationship2}
                    onChange={(e) => {
                        setEmergencyContactRelationship2(e.target.value);
                    }}
                    placeholder="Emergency Contact Relationship (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>

            <div className="mb-6">
                <label className="block mb-1 font-semibold text-gray-700">Emergency Contact Address (2)</label>
                <input
                    type="text"
                    value={emergencyContactAddress2}
                    onChange={(e) => {
                        setEmergencyContactAddress2(e.target.value);
                    }}
                    placeholder="Emergency Contact Address (2)"
                    className="w-full p-2 rounded border-gray-300"
                />
            </div>






            <button onClick={goBack} className="w-full py-2 px-4 mt-2 bg-gray-500 text-white rounded hover:bg-gray-600 transition-colors">Back</button>
            <button onClick={nextStep} className="w-full py-2 px-4 mt-4 bg-blue-500 text-white rounded hover:bg-blue-600 transition-colors">Next</button>
        </div>
    );
}

export default Step9
