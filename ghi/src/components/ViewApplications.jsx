import React, { useEffect, useState } from "react";
import { collection, getDocs, deleteDoc, doc, getDoc } from "firebase/firestore";
import { db } from "../firebase";
import { getAuth, onAuthStateChanged } from "firebase/auth";
import { getStorage, ref, deleteObject } from "firebase/storage";


function ViewApplications() {
    const [applications, setApplications] = useState([]);
    const [userEmail, setUserEmail] = useState("");
    const [searchQuery, setSearchQuery] = useState("");
    const [showConfirmation, setShowConfirmation] = useState(false);
    const [selectedApp, setSelectedApp] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const auth = getAuth();

    useEffect(() => {
        const unsubscribe = onAuthStateChanged(auth, (user) => {
            if (user) {
                setUserEmail(user.email);
            }
            setIsLoading(false);
        });

        return () => unsubscribe();
    }, [auth]);

    useEffect(() => {
        const getApplications = async () => {
            const appCollection = collection(db, "application");
            const appSnapshot = await getDocs(appCollection);
            const appList = appSnapshot.docs.map((doc) => ({
                id: doc.id,
                data: doc.data(),
            }));
            setApplications(appList);
        };

        getApplications();
    }, []);

    // Add a loading message while checking for user
    if (isLoading) {
        return <div>Loading...</div>;
    }

    if (userEmail !== "c.r.zambito@gmail.com" && userEmail !== "fjzllc@gmail.com" && userEmail !== "b.a.zambito@gmail.com") {
        return null;
    }

    const handleDeleteApplication = async (id) => {
        try {
            const applicationDoc = await getDoc(doc(db, "application", id));
            if (!applicationDoc.exists) {
                console.error("No document exists for the provided ID.");
                return;
            }

            const applicationData = applicationDoc.data();

            // Get a reference to the storage service, which is used to create references in your storage bucket
            const storage = getStorage();

            // If photoID exists, delete the associated file
            if (applicationData.photoID) {
                const photoIDRef = ref(storage, applicationData.photoID);
                await deleteObject(photoIDRef);
            }

            // If additionalID exists, delete the associated file
            if (applicationData.additionalID) {
                const additionalIDRef = ref(storage, applicationData.additionalID);
                await deleteObject(additionalIDRef);
            }

            // Delete the document
            await deleteDoc(doc(db, "application", id));

            setApplications((prevApplications) =>
                prevApplications.filter((app) => app.id !== id)
            );
        } catch (error) {
            console.error("Error deleting application: ", error);
        }
    };


    const handleConfirmDelete = (id) => {
        setSelectedApp(id);
        setShowConfirmation(true);
    };

    const handleCancelDelete = () => {
        setSelectedApp(null);
        setShowConfirmation(false);
    };

    const handleConfirmDeleteFinal = () => {
        handleDeleteApplication(selectedApp);
        setShowConfirmation(false);
    };

    const handleSearch = (event) => {
            setSearchQuery(event.target.value);
        };

        const filteredApplications = applications.filter((app) => {
            const firstName = app.data.firstName.toLowerCase();
            const lastName = app.data.lastName.toLowerCase();
            const email = app.data.email.toLowerCase();
            const query = searchQuery.toLowerCase();

            return (
                firstName.includes(query) ||
                lastName.includes(query) ||
                email.includes(query)
                );
            });


        return (
            <div className="container mx-auto py-8">
                <h1 className="text-2xl font-bold mb-4">View Applications</h1>
                <div className="mb-4">
                    <input
                    type="text"
                    placeholder="Search by Name or Email"
                    value={searchQuery}
                    onChange={handleSearch}
                    className="p-2 border rounded"
                    />
                </div>
                {filteredApplications.length === 0 ? (
                    <p>No applications found.</p>
                ) : (
                    <div>
                    {filteredApplications.map((app) => (
                        <div key={app.id} className="my-4">
                        <details className="border rounded-md shadow">
                            <summary className="p-4 cursor-pointer focus:outline-none">
                            Application {app.data.firstName} {app.data.lastName} [
                            {app.data.dateTime}]
                            </summary>
                    <div className="p-4">

                    <p className="mb-2">
                        <span className="font-semibold">Date:</span>{" "}
                        {app.data["dateTime"]}
                    </p>

                    <p className="mb-2">
                        <span className="font-semibold">Apartment:</span>{" "}
                        {app.data["apt"]}
                    </p>

                    <div className="flex items-center mb-4">
                        <div className="flex-1 border-b-2"></div>
                            <span className="px-2 text-lg text-gray-700 font-semibold">Personal Information</span>
                        <div className="flex-1 border-b-2"></div>
                    </div>


                    <p className="mb-2">
                        <span className="font-semibold">First name:</span>{" "}
                        {app.data["firstName"]}
                    </p>

                {app.data["middleName"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Middle name:</span>{" "}
                        {app.data["middleName"]}
                    </p>
                )}

                    <p className="mb-2">
                        <span className="font-semibold">Last name:</span>{" "}
                        {app.data["lastName"]}
                    </p>

                    <p className="mb-2">
                        <span className="font-semibold">Email:</span>{" "}
                        {app.data["email"]}
                    </p>

                    <p className="mb-2">
                        <span className="font-semibold">Phone:</span>{" "}
                        {app.data["phone"]}
                    </p>

                    <p className="mb-2">
                        <span className="font-semibold">SSN:</span>{" "}
                        {app.data["ssn"]}
                    </p>

                    <p className="mb-2">
                        <span className="font-semibold">Drivers License #:</span>{" "}
                        {app.data["driverLicenseNumber"]}
                    </p>

                    <p className="mb-2">
                        <span className="font-semibold">Date Of Birth:</span>{" "}
                        {app.data["dateOfBirth"]}
                    </p>

                    <p className="mb-2">
                        <span className="font-semibold">Marital Status:</span>{" "}
                        {app.data["maritalStatus"]}
                    </p>

                    <p className="mb-2">
                        <span className="font-semibold">Present Home Address:</span>{" "}
                        {app.data["presentHomeAddress"]}
                    </p>

                    <p className="mb-2">
                        <span className="font-semibold">Length Of Time:</span>{" "}
                        {app.data["lengthOfTime"]}
                    </p>

                {app.data["landlordPhone"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Landlord Phone:</span>{" "}
                        {app.data["landlordPhone"]}
                    </p>
                )}

                    <p className="mb-2">
                        <span className="font-semibold">Reason for leaving:</span>{" "}
                        {app.data["reasonForLeaving"]}
                    </p>

                    <p className="mb-2">
                        <span className="font-semibold">Amount of Rent:</span>{" "}
                        {app.data["rent"]}
                    </p>

                    <p className="mb-2">
                        <span className="font-semibold">Is Rent Up To Date?:</span>{" "}
                        {app.data["isRentUpToDate"]}
                    </p>

                    <div className="flex items-center mb-4">
                        <div className="flex-1 border-b-2"></div>
                            <span className="px-2 text-lg text-gray-700 font-semibold">Proposed Occupants</span>
                        <div className="flex-1 border-b-2"></div>
                    </div>


                    <p className="mb-2">
                        <span className="font-semibold">Amount of Occupants:</span>{" "}
                        {app.data["proposedOccupantAmount"]}
                    </p>

                {app.data["occupant1Name"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Occupant 1 Name:</span>{" "}
                        {app.data["occupant1Name"]}
                    </p>
                )}

                {app.data["occupant1Relationship"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Occupant 1 Relationship:</span>{" "}
                        {app.data["occupant1Relationship"]}
                    </p>
                )}

                {app.data["occupant1Occupation"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Occupant 1 Occupation:</span>{" "}
                        {app.data["occupant1Occupation"]}
                    </p>
                )}

                {app.data["occupant1Age"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Occupant 1 Age:</span>{" "}
                        {app.data["occupant1Age"]}
                    </p>
                )}

                {app.data["occupant2Name"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Occupant 2 Name:</span>{" "}
                        {app.data["occupant2Name"]}
                    </p>
                )}

                {app.data["occupant2Relationship"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Occupant 2 Relationship:</span>{" "}
                        {app.data["occupant2Relationship"]}
                    </p>
                )}

                {app.data["occupant2Occupation"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Occupant 2 Occupation:</span>{" "}
                        {app.data["occupant2Occupation"]}
                    </p>
                )}

                {app.data["occupant2Age"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Occupant 2 Age:</span>{" "}
                        {app.data["occupant2Age"]}
                    </p>
                )}

                {app.data["occupant3Name"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Occupant 3 Name:</span>{" "}
                        {app.data["occupant3Name"]}
                    </p>
                )}

                {app.data["occupant3Relationship"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Occupant 3 Relationship:</span>{" "}
                        {app.data["occupant3Relationship"]}
                    </p>
                )}

                {app.data["occupant3Occupation"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Occupant 3 Occupation:</span>{" "}
                        {app.data["occupant3Occupation"]}
                    </p>
                )}

                {app.data["occupant3Age"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Occupant 3 Age:</span>{" "}
                        {app.data["occupant3Age"]}
                    </p>
                )}

                {app.data["occupant4Name"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Occupant 4 Name:</span>{" "}
                        {app.data["occupant4Name"]}
                    </p>
                )}

                {app.data["occupant4Relationship"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Occupant 4 Relationship:</span>{" "}
                        {app.data["occupant4Relationship"]}
                    </p>
                )}

                {app.data["occupant4Occupation"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Occupant 4 Occupation:</span>{" "}
                        {app.data["occupant4Occupation"]}
                    </p>
                )}

                {app.data["occupant4Age"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Occupant 4 Age:</span>{" "}
                        {app.data["occupant4Age"]}
                    </p>
                )}

                {app.data["occupant5Name"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Occupant 5 Name:</span>{" "}
                        {app.data["occupant5Name"]}
                    </p>
                )}

                {app.data["occupant5Relationship"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Occupant 5 Relationship:</span>{" "}
                        {app.data["occupant5Relationship"]}
                    </p>
                )}

                {app.data["occupant5Occupation"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Occupant 5 Occupation:</span>{" "}
                        {app.data["occupant5Occupation"]}
                    </p>
                )}

                {app.data["occupant5Age"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Occupant 5 Age:</span>{" "}
                        {app.data["occupant5Age"]}
                    </p>
                )}


                    <div className="flex items-center mb-4">
                        <div className="flex-1 border-b-2"></div>
                            <span className="px-2 text-lg text-gray-700 font-semibold">Proposed Pets</span>
                        <div className="flex-1 border-b-2"></div>
                    </div>


                {app.data["proposedNumberOfPets"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Proposed Number Of Pets:</span>{" "}
                        {app.data["proposedNumberOfPets"]}
                    </p>
                )}


                {app.data["proposedPetName1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Pet 1 Name:</span>{" "}
                        {app.data["proposedPetName1"]}
                    </p>
                )}

                {app.data["proposedPetBreed1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Pet 1 Breed:</span>{" "}
                        {app.data["proposedPetBreed1"]}
                    </p>
                )}

                {app.data["proposedPetIndoorOutdoor1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Pet 1 Indoor / Outdoor:</span>{" "}
                        {app.data["proposedPetIndoorOutdoor1"]}
                    </p>
                )}

                {app.data["proposedPetAge1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Pet 1 Age:</span>{" "}
                        {app.data["proposedPetAge1"]}
                    </p>
                )}

                {app.data["proposedPetName2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Pet 2 Name:</span>{" "}
                        {app.data["proposedPetName2"]}
                    </p>
                )}

                {app.data["proposedPetBreed2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Pet 2 Breed:</span>{" "}
                        {app.data["proposedPetBreed2"]}
                    </p>
                )}

                {app.data["proposedPetIndoorOutdoor2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Pet 2 Indoor / Outdoor:</span>{" "}
                        {app.data["proposedPetIndoorOutdoor2"]}
                    </p>
                )}

                {app.data["proposedPetAge2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Pet 2 Age:</span>{" "}
                        {app.data["proposedPetAge2"]}
                    </p>
                )}

                {app.data["proposedPetName3"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Pet 3 Name:</span>{" "}
                        {app.data["proposedPetName3"]}
                    </p>
                )}

                {app.data["proposedPetBreed3"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Pet 3 Breed:</span>{" "}
                        {app.data["proposedPetBreed3"]}
                    </p>
                )}

                {app.data["proposedPetIndoorOutdoor3"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Pet 3 Indoor / Outdoor:</span>{" "}
                        {app.data["proposedPetIndoorOutdoor3"]}
                    </p>
                )}

                {app.data["proposedPetAge3"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Pet 3 Age:</span>{" "}
                        {app.data["proposedPetAge3"]}
                    </p>
                )}


                    <div className="flex items-center mb-4">
                        <div className="flex-1 border-b-2"></div>
                            <span className="px-2 text-lg text-gray-700 font-semibold">Vehicle Information</span>
                        <div className="flex-1 border-b-2"></div>
                    </div>


                {app.data["vehicleAmount"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Number of Vehicles:</span>{" "}
                        {app.data["vehicleAmount"]}
                    </p>
                )}

                {app.data["vehicleYear1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Vehicle 1 Year:</span>{" "}
                        {app.data["vehicleYear1"]}
                    </p>
                )}

                {app.data["vehicleMake1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Vehicle 1 Make:</span>{" "}
                        {app.data["vehicleMake1"]}
                    </p>
                )}

                {app.data["vehicleModel1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Vehicle 1 Model:</span>{" "}
                        {app.data["vehicleModel1"]}
                    </p>
                )}

                {app.data["vehicleColor1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Vehicle 1 Color:</span>{" "}
                        {app.data["vehicleColor1"]}
                    </p>
                )}

                {app.data["vehiclePlateNumber1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Vehicle 1 Plate:</span>{" "}
                        {app.data["vehiclePlateNumber1"]}
                    </p>
                )}

                {app.data["vehiclePlateState1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Vehicle 1 State:</span>{" "}
                        {app.data["vehiclePlateState1"]}
                    </p>
                )}

                {app.data["vehicleYear2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Vehicle 2 Year:</span>{" "}
                        {app.data["vehicleYear2"]}
                    </p>
                )}

                {app.data["vehicleMake2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Vehicle 2 Make:</span>{" "}
                        {app.data["vehicleMake2"]}
                    </p>
                )}

                {app.data["vehicleModel2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Vehicle 2 Model:</span>{" "}
                        {app.data["vehicleModel2"]}
                    </p>
                )}

                {app.data["vehicleColor2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Vehicle 2 Color:</span>{" "}
                        {app.data["vehicleColor2"]}
                    </p>
                )}

                {app.data["vehiclePlateNumber2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Vehicle 2 Plate:</span>{" "}
                        {app.data["vehiclePlateNumber2"]}
                    </p>
                )}

                {app.data["vehiclePlateState2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Vehicle 2 State:</span>{" "}
                        {app.data["vehiclePlateState2"]}
                    </p>
                )}


                    <div className="flex items-center mb-4">
                        <div className="flex-1 border-b-2"></div>
                            <span className="px-2 text-lg text-gray-700 font-semibold">Employment Information</span>
                        <div className="flex-1 border-b-2"></div>
                    </div>


                {app.data["employmentStatus"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Current Employer (1):</span>{" "}
                        {app.data["employmentStatus"]}
                    </p>
                )}

                {app.data["employmentCurrentEmployer1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Current Employer (1):</span>{" "}
                        {app.data["employmentCurrentEmployer1"]}
                    </p>
                )}

                {app.data["employmentOccupation1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Employment Occupation (1):</span>{" "}
                        {app.data["employmentOccupation1"]}
                    </p>
                )}

                {app.data["employmentHoursWeek1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Employment Hours/Week (1):</span>{" "}
                        {app.data["employmentHoursWeek1"]}
                    </p>
                )}

                {app.data["employmentSupervisor1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Employment Supervisor (1):</span>{" "}
                        {app.data["employmentSupervisor1"]}
                    </p>
                )}

                {app.data["employmentPhone1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Employment Phone (1):</span>{" "}
                        {app.data["employmentPhone1"]}
                    </p>
                )}

                {app.data["employmentYearsEmployed1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Employment Years Employed (1):</span>{" "}
                        {app.data["employmentYearsEmployed1"]}
                    </p>
                )}

                {app.data["employmentAddress1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Employment Address (1):</span>{" "}
                        {app.data["employmentAddress1"]}
                    </p>
                )}

                {app.data["employmentCurrentEmployer2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Current Employer (2):</span>{" "}
                        {app.data["employmentCurrentEmployer2"]}
                    </p>
                )}

                {app.data["employmentOccupation2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Employment Occupation (2):</span>{" "}
                        {app.data["employmentOccupation2"]}
                    </p>
                )}

                {app.data["employmentHoursWeek2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Employment Hours/Week (2):</span>{" "}
                        {app.data["employmentHoursWeek2"]}
                    </p>
                )}

                {app.data["employmentSupervisor2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Employment Supervisor (2):</span>{" "}
                        {app.data["employmentSupervisor2"]}
                    </p>
                )}

                {app.data["employmentPhone2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Employment Phone (2):</span>{" "}
                        {app.data["employmentPhone2"]}
                    </p>
                )}

                {app.data["employmentYearsEmployed2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Employment Years Employed (2):</span>{" "}
                        {app.data["employmentYearsEmployed2"]}
                    </p>
                )}

                {app.data["employmentAddress2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Employment Address (2):</span>{" "}
                        {app.data["employmentAddress2"]}
                    </p>
                )}


                    <div className="flex items-center mb-4">
                        <div className="flex-1 border-b-2"></div>
                            <span className="px-2 text-lg text-gray-700 font-semibold">Income Information</span>
                        <div className="flex-1 border-b-2"></div>
                    </div>


                {app.data["currentIncome1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Current Income (1):</span>{" "}
                        {app.data["currentIncome1"]}
                    </p>
                )}

                {app.data["currentIncomeAmount1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Current Income Amount (1):</span>{" "}
                        {app.data["currentIncomeAmount1"]}
                    </p>
                )}

                {app.data["currentIncomeSource1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Current Income Source (1):</span>{" "}
                        {app.data["currentIncomeSource1"]}
                    </p>
                )}

                {app.data["currentProofOfIncome1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Proof Of Income (YES / NO) (1):</span>{" "}
                        {app.data["currentProofOfIncome1"]}
                    </p>
                )}

                {app.data["currentIncome2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Current Income (2):</span>{" "}
                        {app.data["currentIncome2"]}
                    </p>
                )}

                {app.data["currentIncomeAmount2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Current Income Amount (2):</span>{" "}
                        {app.data["currentIncomeAmount2"]}
                    </p>
                )}

                {app.data["currentIncomeSource2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Current Income Source (2):</span>{" "}
                        {app.data["currentIncomeSource2"]}
                    </p>
                )}

                {app.data["currentProofOfIncome2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Proof Of Income (YES / NO) (2):</span>{" "}
                        {app.data["currentProofOfIncome2"]}
                    </p>
                )}



                    <div className="flex items-center mb-4">
                        <div className="flex-1 border-b-2"></div>
                            <span className="px-2 text-lg text-gray-700 font-semibold">Credit Card / Financial Information</span>
                        <div className="flex-1 border-b-2"></div>
                    </div>


                    {app.data["carLoan"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Car Loan/Lien Holder:</span>{" "}
                        {app.data["carLoan"]}
                    </p>
                )}

                {app.data["carLoanBalanceOwed"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Car Loan Balance Owed:</span>{" "}
                        {app.data["carLoanBalanceOwed"]}
                    </p>
                )}

                {app.data["carLoanMonthlyPayment"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Car Loan Monthly Payment:</span>{" "}
                        {app.data["carLoanMonthlyPayment"]}
                    </p>
                )}

                {app.data["carLoanCreditorPhone"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Car Loan Creditor Phone Number:</span>{" "}
                        {app.data["carLoanCreditorPhone"]}
                    </p>
                )}


                {app.data["creditCardCompany1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Credit Card Company (1):</span>{" "}
                        {app.data["creditCardCompany1"]}
                    </p>
                )}

                {app.data["creditCardCompanyBalanceOwed1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Credit Card Balance (1):</span>{" "}
                        {app.data["creditCardCompanyBalanceOwed1"]}
                    </p>
                )}

                {app.data["creditCardCompanyMonthlyPayment1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Credit Card Monthly Payment (1):</span>{" "}
                        {app.data["creditCardCompanyMonthlyPayment1"]}
                    </p>
                )}

                {app.data["creditCardCompanyCreditorPhone1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Creditor Phone (1):</span>{" "}
                        {app.data["creditCardCompanyCreditorPhone1"]}
                    </p>
                )}

                {app.data["creditCardCompany2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Credit Card Company (2):</span>{" "}
                        {app.data["creditCardCompany2"]}
                    </p>
                )}

                {app.data["creditCardCompanyBalanceOwed2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Credit Card Balance (2):</span>{" "}
                        {app.data["creditCardCompanyBalanceOwed2"]}
                    </p>
                )}

                {app.data["creditCardCompanyMonthlyPayment2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Credit Card Monthly Payment (2):</span>{" "}
                        {app.data["creditCardCompanyMonthlyPayment2"]}
                    </p>
                )}

                {app.data["creditCardCompanyCreditorPhone2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Creditor Phone (2):</span>{" "}
                        {app.data["creditCardCompanyCreditorPhone2"]}
                    </p>
                )}

                {app.data["creditCardCompany3"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Credit Card Company (3):</span>{" "}
                        {app.data["creditCardCompany3"]}
                    </p>
                )}

                {app.data["creditCardCompanyBalanceOwed3"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Credit Card Balance (3):</span>{" "}
                        {app.data["creditCardCompanyBalanceOwed3"]}
                    </p>
                )}

                {app.data["creditCardCompanyMonthlyPayment3"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Credit Card Monthly Payment (3):</span>{" "}
                        {app.data["creditCardCompanyMonthlyPayment3"]}
                    </p>
                )}

                {app.data["creditCardCompanyCreditorPhone3"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Creditor Phone (3):</span>{" "}
                        {app.data["creditCardCompanyCreditorPhone3"]}
                    </p>
                )}

                {app.data["childSupportOther"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Child Support / Other Creditor Owed:</span>{" "}
                        {app.data["childSupportOther"]}
                    </p>
                )}

                {app.data["childSupportOtherBalance"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Child Support / Other Balance Owed:</span>{" "}
                        {app.data["childSupportOtherBalance"]}
                    </p>
                )}

                {app.data["childSupportOtherMonthlyPayment"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Child Support / Other Monthly Payment:</span>{" "}
                        {app.data["childSupportOtherMonthlyPayment"]}
                    </p>
                )}

                {app.data["childSupportOtherCreditorPhone"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Child Support / Other Creditor Phone:</span>{" "}
                        {app.data["childSupportOtherCreditorPhone"]}
                    </p>
                )}

                {app.data["bankAccountName"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Bank Account Name Of Bank:</span>{" "}
                        {app.data["bankAccountName"]}
                    </p>
                )}

                {app.data["bankAccountBalance"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Bank Account Balance:</span>{" "}
                        {app.data["bankAccountBalance"]}
                    </p>
                )}

                {app.data["bankAccountMonthlyPayment"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Bank Account Monthly Payment:</span>{" "}
                        {app.data["bankAccountMonthlyPayment"]}
                    </p>
                )}

                {app.data["bankAccountNumber"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Bank Account Number:</span>{" "}
                        {app.data["bankAccountNumber"]}
                    </p>
                )}


                    <div className="flex items-center mb-4">
                        <div className="flex-1 border-b-2"></div>
                            <span className="px-2 text-lg text-gray-700 font-semibold">Emergency Contact</span>
                        <div className="flex-1 border-b-2"></div>
                    </div>


                {app.data["emergencyContact1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Emergency Contact (1):</span>{" "}
                        {app.data["emergencyContact1"]}
                    </p>
                )}

                {app.data["emergencyContactPhone1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Emergency Contact Phone (1):</span>{" "}
                        {app.data["emergencyContactPhone1"]}
                    </p>
                )}

                {app.data["emergencyContactRelationship1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Emergency Contact Relationship (1):</span>{" "}
                        {app.data["emergencyContactRelationship1"]}
                    </p>
                )}

                {app.data["emergencyContactAddress1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Emergency Contact Address (1):</span>{" "}
                        {app.data["emergencyContactAddress1"]}
                    </p>
                )}

                {app.data["emergencyContact2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Emergency Contact (2):</span>{" "}
                        {app.data["emergencyContact2"]}
                    </p>
                )}

                {app.data["emergencyContactPhone2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Emergency Contact Phone (2):</span>{" "}
                        {app.data["emergencyContactPhone2"]}
                    </p>
                )}

                {app.data["emergencyContactRelationship2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Emergency Contact Relationship (2):</span>{" "}
                        {app.data["emergencyContactRelationship2"]}
                    </p>
                )}

                {app.data["emergencyContactAddress2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Emergency Contact Address (2):</span>{" "}
                        {app.data["emergencyContactAddress2"]}
                    </p>
                )}


                    <div className="flex items-center mb-4">
                        <div className="flex-1 border-b-2"></div>
                            <span className="px-2 text-lg text-gray-700 font-semibold">Personal Reference</span>
                        <div className="flex-1 border-b-2"></div>
                    </div>


                {app.data["referenceContact1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Personal Reference Contact (1):</span>{" "}
                        {app.data["referenceContact1"]}
                    </p>
                )}

                {app.data["referenceContactPhone1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Personal Reference Phone (1):</span>{" "}
                        {app.data["referenceContactPhone1"]}
                    </p>
                )}

                {app.data["referenceContactRelationship1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Personal Reference Relationship (1):</span>{" "}
                        {app.data["referenceContactRelationship1"]}
                    </p>
                )}

                {app.data["referenceContactAddress1"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Personal Reference Address (1):</span>{" "}
                        {app.data["referenceContactAddress1"]}
                    </p>
                )}

                {app.data["referenceContact2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Personal Reference Contact (2):</span>{" "}
                        {app.data["referenceContact2"]}
                    </p>
                )}

                {app.data["referenceContactPhone2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Personal Reference Phone (2):</span>{" "}
                        {app.data["referenceContactPhone2"]}
                    </p>
                )}

                {app.data["referenceContactRelationship2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Personal Reference Relationship (2):</span>{" "}
                        {app.data["referenceContactRelationship2"]}
                    </p>
                )}

                {app.data["referenceContactAddress2"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Personal Reference Address (2):</span>{" "}
                        {app.data["referenceContactAddress2"]}
                    </p>
                )}


                    <div className="flex items-center mb-4">
                        <div className="flex-1 border-b-2"></div>
                            <span className="px-2 text-lg text-gray-700 font-semibold">Applicant Questionnaire</span>
                        <div className="flex-1 border-b-2"></div>
                    </div>


                {app.data["applicantSued"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Has Applicant Ever Been Sued For Bills?:</span>{" "}
                        {app.data["applicantSued"]}
                    </p>
                )}

                {app.data["applicantBankrupt"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Has Applicant Ever Been Bankrupt?:</span>{" "}
                        {app.data["applicantBankrupt"]}
                    </p>
                )}

                {app.data["applicantFelony"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Has Applicant Ever Been Guilty Of A Felony?:</span>{" "}
                        {app.data["applicantFelony"]}
                    </p>
                )}

                {app.data["applicantBroken"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Has Applicant Ever Broken A Lease?:</span>{" "}
                        {app.data["applicantBroken"]}
                    </p>
                )}

                {app.data["applicantSheriff"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Has Applicant Ever Been Locked Out Of Their Apartment By The Sheriff?:</span>{" "}
                        {app.data["applicantSheriff"]}
                    </p>
                )}

                {app.data["applicantCourt"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Has Applicant Ever Been Brought To Court By Another Landlord?:</span>{" "}
                        {app.data["applicantCourt"]}
                    </p>
                )}

                {app.data["applicantMoved"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Has Applicant Ever Moved Owing Rent Or Damaged An Apartment?:</span>{" "}
                        {app.data["applicantMoved"]}
                    </p>
                )}

                {app.data["applicantRent"] && (
                    <p className="mb-2">
                        <span className="font-semibold">Is The Total Move-In Amount Available Now (Rent And Deposit)?:</span>{" "}
                        {app.data["applicantRent"]}
                    </p>
                )}


                    <div className="flex items-center mb-4">
                        <div className="flex-1 border-b-2"></div>
                            <span className="px-2 text-lg text-gray-700 font-semibold">Applicant Photo ID</span>
                        <div className="flex-1 border-b-2"></div>
                    </div>


                    <p className="mb-2">
                        <span className="font-semibold">ID:</span><br />
                        <img src={app.data["photoID"]} alt="ID" />
                    </p>


                    <div className="flex items-center mb-4">
                        <div className="flex-1 border-b-2"></div>
                            <span className="px-2 text-lg text-gray-700 font-semibold">Applicant Authorization</span>
                        <div className="flex-1 border-b-2"></div>
                    </div>


                    <p className="mb-2">
                        <span className="font-semibold">Signature:</span>{" "}
                        {app.data["signature"]}
                    </p>

                    <p className="mb-2">
                        <span className="font-semibold">Date Signed:</span>{" "}
                        {app.data["date"]}
                    </p>







                    <div className="flex mt-4">
                        <button
                        onClick={() => handleConfirmDelete(app.id)}
                        className="mr-2 bg-red-500 hover:bg-red-600 text-white px-4 py-2 rounded focus:outline-none"
                        >
                        Delete
                        </button>
                    </div>
                    </div>
                </details>
                </div>
            ))}
            </div>
        )}

        {showConfirmation && (
            <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50">
            <div className="bg-white w-96 rounded-lg p-6">
                <p className="mb-4">Are you sure you want to delete this application?</p>
                <div className="flex justify-end">
                <button
                    onClick={handleCancelDelete}
                    className="mr-2 bg-gray-300 hover:bg-gray-400 text-gray-800 px-4 py-2 rounded focus:outline-none"
                >
                    Cancel
                </button>
                <button
                    onClick={handleConfirmDeleteFinal}
                    className="bg-red-500 hover:bg-red-600 text-white px-4 py-2 rounded focus:outline-none"
                >
                    Delete
                </button>
                </div>
            </div>
            </div>
        )}
        </div>
    );
}

export default ViewApplications;





















// import React, { useEffect, useState } from "react";
// import { collection, getDocs, deleteDoc, doc } from "firebase/firestore";
// import { db } from "../firebase";
// import { getAuth, onAuthStateChanged } from "firebase/auth";

// function ViewApplications() {
//     const [applications, setApplications] = useState([]);
//     const [userEmail, setUserEmail] = useState("");
//     const [showConfirmation, setShowConfirmation] = useState(false);
//     const [selectedApp, setSelectedApp] = useState(null);
//     const [isLoading, setIsLoading] = useState(true);
//     const auth = getAuth();

//     useEffect(() => {
//         const unsubscribe = onAuthStateChanged(auth, (user) => {
//             if (user) {
//                 setUserEmail(user.email);
//             }
//             setIsLoading(false);
//         });

//         return () => unsubscribe();
//     }, [auth]);

//     useEffect(() => {
//         const getApplications = async () => {
//             const appCollection = collection(db, "application");
//             const appSnapshot = await getDocs(appCollection);
//             const appList = appSnapshot.docs.map((doc) => ({
//                 id: doc.id,
//                 data: doc.data(),
//             }));
//             setApplications(appList);
//         };

//         getApplications();
//     }, []);

//     // Add a loading message while checking for user
//     if (isLoading) {
//         return <div>Loading...</div>;
//     }

//     if (userEmail !== "c.r.zambito@gmail.com") {
//         return null;
//     }

//     const handleDeleteApplication = async (id) => {
//         try {
//         await deleteDoc(doc(db, "application", id));
//         setApplications((prevApplications) =>
//             prevApplications.filter((app) => app.id !== id)
//         );
//         } catch (error) {
//         console.error("Error deleting application: ", error);
//         }
//     };

//     const handleConfirmDelete = (id) => {
//         setSelectedApp(id);
//         setShowConfirmation(true);
//     };

//     const handleCancelDelete = () => {
//         setSelectedApp(null);
//         setShowConfirmation(false);
//     };

//     const handleConfirmDeleteFinal = () => {
//         handleDeleteApplication(selectedApp);
//         setShowConfirmation(false);
//     };

//     return (
//         <div className="container mx-auto py-8">
//         <h1 className="text-2xl font-bold mb-4">View Applications</h1>
//         {applications.length === 0 ? (
//             <p>No applications found.</p>
//         ) : (
//             <div>
//             {applications.map((app) => (
//                 <div key={app.id} className="my-4">
//                 <details className="border rounded-md shadow">
//                     <summary className="p-4 cursor-pointer focus:outline-none">
//                     Application {app.data["firstName"]} {app.data["lastName"]} [{app.data["dateTime"]}]
//                     </summary>
//                     <div className="p-4">

//                     <p className="mb-2">
//                         <span className="font-semibold">Date:</span>{" "}
//                         {app.data["dateTime"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Apartment:</span>{" "}
//                         {app.data["apt"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">First name:</span>{" "}
//                         {app.data["firstName"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Middle name:</span>{" "}
//                         {app.data["middleName"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Last name:</span>{" "}
//                         {app.data["lastName"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Email:</span>{" "}
//                         {app.data["email"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Phone:</span>{" "}
//                         {app.data["phone"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">SSN:</span>{" "}
//                         {app.data["ssn"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Drivers License #:</span>{" "}
//                         {app.data["driverLicenseNumber"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Date Of Birth:</span>{" "}
//                         {app.data["dateOfBirth"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Marital Status:</span>{" "}
//                         {app.data["maritalStatus"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Present Home Address:</span>{" "}
//                         {app.data["presentHomeAddress"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Length Of Time:</span>{" "}
//                         {app.data["lengthOfTime"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Landlord Phone:</span>{" "}
//                         {app.data["landlordPhone"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Reason for leaving:</span>{" "}
//                         {app.data["reasonForLeaving"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Amount of Rent:</span>{" "}
//                         {app.data["rent"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Is Rent Up To Date?:</span>{" "}
//                         {app.data["isRentUpToDate"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Amount of Occupants:</span>{" "}
//                         {app.data["proposedOccupantAmount"]}
//                     </p>





//                     <p className="mb-2">
//                         <span className="font-semibold">Occupant 1 Name:</span>{" "}
//                         {app.data["occupant1Name"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Occupant 1 Relationship:</span>{" "}
//                         {app.data["occupant1Relationship"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Occupant 1 Occupation:</span>{" "}
//                         {app.data["occupant1Occupation"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Occupant 1 Age:</span>{" "}
//                         {app.data["occupant1Age"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Occupant 2 Name:</span>{" "}
//                         {app.data["occupant2Name"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Occupant 2 Relationship:</span>{" "}
//                         {app.data["occupant2Relationship"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Occupant 2 Occupation:</span>{" "}
//                         {app.data["occupant2Occupation"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Occupant 2 Age:</span>{" "}
//                         {app.data["occupant2Age"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Occupant 3 Name:</span>{" "}
//                         {app.data["occupant3Name"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Occupant 3 Relationship:</span>{" "}
//                         {app.data["occupant3Relationship"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Occupant 3 Occupation:</span>{" "}
//                         {app.data["occupant3Occupation"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Occupant 3 Age:</span>{" "}
//                         {app.data["occupant3Age"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Occupant 4 Name:</span>{" "}
//                         {app.data["occupant4Name"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Occupant 4 Relationship:</span>{" "}
//                         {app.data["occupant4Relationship"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Occupant 4 Occupation:</span>{" "}
//                         {app.data["occupant4Occupation"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Occupant 4 Age:</span>{" "}
//                         {app.data["occupant4Age"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Occupant 5 Name:</span>{" "}
//                         {app.data["occupant5Name"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Occupant 5 Relationship:</span>{" "}
//                         {app.data["occupant5Relationship"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Occupant 5 Occupation:</span>{" "}
//                         {app.data["occupant5Occupation"]}
//                     </p>

//                     <p className="mb-2">
//                         <span className="font-semibold">Occupant 5 Age:</span>{" "}
//                         {app.data["occupant5Age"]}
//                     </p>




//                     <div className="flex mt-4">
//                         <button
//                         onClick={() => handleConfirmDelete(app.id)}
//                         className="mr-2 bg-red-500 hover:bg-red-600 text-white px-4 py-2 rounded focus:outline-none"
//                         >
//                         Delete
//                         </button>
//                     </div>
//                     </div>
//                 </details>
//                 </div>
//             ))}
//             </div>
//         )}

//         {showConfirmation && (
//             <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50">
//             <div className="bg-white w-96 rounded-lg p-6">
//                 <p className="mb-4">Are you sure you want to delete this application?</p>
//                 <div className="flex justify-end">
//                 <button
//                     onClick={handleCancelDelete}
//                     className="mr-2 bg-gray-300 hover:bg-gray-400 text-gray-800 px-4 py-2 rounded focus:outline-none"
//                 >
//                     Cancel
//                 </button>
//                 <button
//                     onClick={handleConfirmDeleteFinal}
//                     className="bg-red-500 hover:bg-red-600 text-white px-4 py-2 rounded focus:outline-none"
//                 >
//                     Delete
//                 </button>
//                 </div>
//             </div>
//             </div>
//         )}
//         </div>
//     );
// }

// export default ViewApplications;
