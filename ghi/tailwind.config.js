/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      width: {
        '1/13': '7.69231%',
        '2/13': '15.38462%',
        '3/13': '23.07692%',
        '4/13': '30.76923%',
        '5/13': '38.46154%',
        '6/13': '46.15385%',
        '7/13': '53.84615%',
        '8/13': '61.53846%',
        '9/13': '69.23077%',
        '10/13': '76.92308%',
        '11/13': '84.61538%',
        '12/13': '92.30769%',
      },
    },
  },
  plugins: [],
}



// /** @type {import('tailwindcss').Config} */
// export default {
//   content: [
//     "./index.html",
//     "./src/**/*.{js,ts,jsx,tsx}",
//   ],
//   theme: {
//     extend: {},
//   },
//   plugins: [],
// }
